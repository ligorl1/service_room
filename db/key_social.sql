-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2017 г., 09:03
-- Версия сервера: 10.1.21-MariaDB
-- Версия PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `key_social`
--

-- --------------------------------------------------------

--
-- Структура таблицы `email`
--

CREATE TABLE `email` (
  `email` varchar(50) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `email`
--

INSERT INTO `email` (`email`, `id`) VALUES
('admin@example.net', 56),
('ameliepoulain-92@dzencode.com', 34),
('back-end-d@time-ismoney.com', 35),
('back-end-f@dzencode.com', 37),
('baksspro@dzencode.com', 38),
('ekaterynamytrofanova@dzencode.com', 40),
('galitsky_e.o@dzencode.com', 41),
('holly-colors@time-ismoney.com', 42),
('idealist-guru@time-ismoney.com', 43),
('julkomleva@dzencode.com', 44),
('kuwkuw@dzencode.com', 45),
('liture.ua@time-ismoney.com', 46),
('manager-a@time-ismoney.com', 47),
('natalishapov@dzencode.com', 48),
('nogenkoworkmail@dzencode.com', 36),
('osirus@dzencode.com', 49),
('painkiler.ua@dzencode.com', 50),
('r.izvarina@dzencode.com', 51),
('roma@dzencode.com', 52),
('spice-promo@time-ismoney.com', 53),
('sravnizaim@time-ismoney.com', 54),
('suleyman@dzencode.com', 55),
('test@mail.ru', 33),
('Voodoo773@gmail.com', 39),
('yddmat@dzencode.com', 57);

-- --------------------------------------------------------

--
-- Структура таблицы `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service`
--

INSERT INTO `service` (`id`, `name`) VALUES
(6, 'https://redmine.time-ismoney.com'),
(1, 'inventory.com'),
(3, 'isMoney.com');

-- --------------------------------------------------------

--
-- Структура таблицы `service_connect`
--

CREATE TABLE `service_connect` (
  `id` int(11) NOT NULL,
  `connect` varchar(255) NOT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `room_hash` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service_connect`
--

INSERT INTO `service_connect` (`id`, `connect`, `api_key`, `user_id`, `room_hash`, `url`) VALUES
(5, 'redmine', '989adb1b2b4175293247b537068567829ab47423', 313, 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 'https://redmine.time-ismoney.com'),
(6, 'redmine', '989adb1b2b4175293247b537068567829ab47423', 313, 'aXNNb25leS5jb21yb29tMzMxMw==', 'https://redmine.time-ismoney.com');

-- --------------------------------------------------------

--
-- Структура таблицы `service_email`
--

CREATE TABLE `service_email` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_c` datetime NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service_email`
--

INSERT INTO `service_email` (`id`, `email`, `hash`, `user_id`, `date_c`, `token`) VALUES
(55, 'ameliepoulain-92@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'V3bBYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2FtZWxpZXBvdWxhaW4tOTJAZHplbmNvZGUuY29t'),
(56, 'back-end-d@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'Us4uYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2JhY2stZW5kLWRAdGltZS1pc21vbmV5LmNvbQ=='),
(57, 'nogenkoworkmail@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'nnGPYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N25vZ2Vua293b3JrbWFpbEBkemVuY29kZS5jb20='),
(58, 'back-end-f@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'dEPLYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2JhY2stZW5kLWZAZHplbmNvZGUuY29t'),
(59, 'baksspro@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'EtsgYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2Jha3NzcHJvQGR6ZW5jb2RlLmNvbQ=='),
(60, 'Voodoo773@gmail.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'Jg77YUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N1Zvb2Rvbzc3M0BnbWFpbC5jb20='),
(61, 'ekaterynamytrofanova@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'GHF/YUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2VrYXRlcnluYW15dHJvZmFub3ZhQGR6ZW5jb2RlLmNvbQ=='),
(62, 'galitsky_e.o@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'KHPMYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2dhbGl0c2t5X2Uub0BkemVuY29kZS5jb20='),
(63, 'holly-colors@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'YM/1YUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2hvbGx5LWNvbG9yc0B0aW1lLWlzbW9uZXkuY29t'),
(64, 'idealist-guru@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'y31nYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2lkZWFsaXN0LWd1cnVAdGltZS1pc21vbmV5LmNvbQ=='),
(65, 'julkomleva@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'h2/IYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2p1bGtvbWxldmFAZHplbmNvZGUuY29t'),
(66, 'kuwkuw@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'Rp0uYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2t1d2t1d0BkemVuY29kZS5jb20='),
(67, 'liture.ua@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'XHxuYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2xpdHVyZS51YUB0aW1lLWlzbW9uZXkuY29t'),
(68, 'manager-a@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'KvqOYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N21hbmFnZXItYUB0aW1lLWlzbW9uZXkuY29t'),
(69, 'natalishapov@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'f+i2YUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N25hdGFsaXNoYXBvdkBkemVuY29kZS5jb20='),
(70, 'osirus@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', '6UnkYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N29zaXJ1c0BkemVuY29kZS5jb20='),
(71, 'painkiler.ua@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'h41xYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3BhaW5raWxlci51YUBkemVuY29kZS5jb20='),
(72, 'r.izvarina@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'ZqpHYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3IuaXp2YXJpbmFAZHplbmNvZGUuY29t'),
(73, 'roma@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'SXH4YUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3JvbWFAZHplbmNvZGUuY29t'),
(74, 'spice-promo@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'E7KyYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3NwaWNlLXByb21vQHRpbWUtaXNtb25leS5jb20='),
(75, 'sravnizaim@time-ismoney.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'yFWYYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3NyYXZuaXphaW1AdGltZS1pc21vbmV5LmNvbQ=='),
(76, 'suleyman@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'siImYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3N1bGV5bWFuQGR6ZW5jb2RlLmNvbQ=='),
(77, 'admin@example.net', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', 'XK6uYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N2FkbWluQGV4YW1wbGUubmV0'),
(78, 'yddmat@dzencode.com', 'aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 313, '2017-05-15 08:35:57', '7I2tYUhSMGNITTZMeTl5WldSdGFXNWxMblJwYldVdGFYTnRiMjVsZVM1amIyMXBiblpsYm5SdmNua3pNVE09MjAxNy0wNS0xNSAwODozNTo1N3lkZG1hdEBkemVuY29kZS5jb20='),
(79, 'ameliepoulain-92@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'Ij47YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdhbWVsaWVwb3VsYWluLTkyQGR6ZW5jb2RlLmNvbQ=='),
(80, 'back-end-d@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', '4396YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdiYWNrLWVuZC1kQHRpbWUtaXNtb25leS5jb20='),
(81, 'nogenkoworkmail@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'TB72YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdub2dlbmtvd29ya21haWxAZHplbmNvZGUuY29t'),
(82, 'back-end-f@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'q3T5YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdiYWNrLWVuZC1mQGR6ZW5jb2RlLmNvbQ=='),
(83, 'baksspro@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', '7o8nYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdiYWtzc3Byb0BkemVuY29kZS5jb20='),
(84, 'Voodoo773@gmail.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'n5oJYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdWb29kb283NzNAZ21haWwuY29t'),
(85, 'ekaterynamytrofanova@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'TPLrYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdla2F0ZXJ5bmFteXRyb2Zhbm92YUBkemVuY29kZS5jb20='),
(86, 'galitsky_e.o@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'hOoMYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdnYWxpdHNreV9lLm9AZHplbmNvZGUuY29t'),
(87, 'holly-colors@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'baN4YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdob2xseS1jb2xvcnNAdGltZS1pc21vbmV5LmNvbQ=='),
(88, 'idealist-guru@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'CNROYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdpZGVhbGlzdC1ndXJ1QHRpbWUtaXNtb25leS5jb20='),
(89, 'julkomleva@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'JCIiYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdqdWxrb21sZXZhQGR6ZW5jb2RlLmNvbQ=='),
(90, 'kuwkuw@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', '6N4vYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdrdXdrdXdAZHplbmNvZGUuY29t'),
(91, 'liture.ua@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'RDHuYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdsaXR1cmUudWFAdGltZS1pc21vbmV5LmNvbQ=='),
(92, 'manager-a@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', '5RubYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdtYW5hZ2VyLWFAdGltZS1pc21vbmV5LmNvbQ=='),
(93, 'natalishapov@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', '8s/hYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDduYXRhbGlzaGFwb3ZAZHplbmNvZGUuY29t'),
(94, 'osirus@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'EqhhYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdvc2lydXNAZHplbmNvZGUuY29t'),
(95, 'painkiler.ua@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'C8LPYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdwYWlua2lsZXIudWFAZHplbmNvZGUuY29t'),
(96, 'r.izvarina@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'NfykYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdyLml6dmFyaW5hQGR6ZW5jb2RlLmNvbQ=='),
(97, 'roma@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'ofALYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdyb21hQGR6ZW5jb2RlLmNvbQ=='),
(98, 'spice-promo@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'KPOlYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdzcGljZS1wcm9tb0B0aW1lLWlzbW9uZXkuY29t'),
(99, 'sravnizaim@time-ismoney.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'gpxmYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdzcmF2bml6YWltQHRpbWUtaXNtb25leS5jb20='),
(100, 'suleyman@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'xxtUYVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdzdWxleW1hbkBkemVuY29kZS5jb20='),
(101, 'admin@example.net', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'Fsi9YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDdhZG1pbkBleGFtcGxlLm5ldA=='),
(102, 'yddmat@dzencode.com', 'aXNNb25leS5jb21yb29tMzMxMw==', 313, '2017-05-15 13:00:47', 'yGg9YVhOTmIyNWxlUzVqYjIxeWIyOXRNek14TXc9PTIwMTctMDUtMTUgMTM6MDA6NDd5ZGRtYXRAZHplbmNvZGUuY29t');

-- --------------------------------------------------------

--
-- Структура таблицы `service_image`
--

CREATE TABLE `service_image` (
  `id` int(11) NOT NULL,
  `service_email_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service_image`
--

INSERT INTO `service_image` (`id`, `service_email_id`, `image`, `date`) VALUES
(64, 60, 'Screenshot_0_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(65, 60, 'Screenshot_1_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(66, 60, 'Screenshot_2_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(67, 60, 'Screenshot_3_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(68, 60, 'Screenshot_4_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(69, 60, 'Screenshot_5_2017_05_17_16_58_39.jpg', '2017-05-17 16:58:39'),
(70, 60, 'Screenshot_0_2017_05_17_17_06_07.jpg', '2017-05-17 17:06:07'),
(71, 60, 'Screenshot_1_2017_05_17_17_06_07.jpg', '2017-05-17 17:06:07'),
(72, 60, 'Screenshot_2_2017_05_17_17_06_07.jpg', '2017-05-17 17:06:07'),
(73, 60, 'Screenshot_3_2017_05_17_17_06_07.jpg', '2017-05-17 17:06:07'),
(74, 60, 'Screenshot_4_2017_05_17_17_06_07.jpg', '2017-05-17 17:06:07'),
(75, 60, 'Screenshot_0_2017_05_19_07_15_00.jpg', '2017-05-19 07:15:00'),
(76, 60, 'Screenshot_1_2017_05_19_07_15_00.jpg', '2017-05-19 07:15:00'),
(77, 60, 'Screenshot_2_2017_05_19_07_15_00.jpg', '2017-05-19 07:15:00'),
(78, 60, 'Screenshot_3_2017_05_19_07_15_00.jpg', '2017-05-19 07:15:00'),
(79, 60, 'Screenshot_4_2017_05_19_07_15_00.jpg', '2017-05-19 07:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `service_room`
--

CREATE TABLE `service_room` (
  `hash` varchar(255) NOT NULL,
  `service` int(11) NOT NULL,
  `service_room_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `service_room`
--

INSERT INTO `service_room` (`hash`, `service`, `service_room_name`, `user_id`) VALUES
('aHR0cHM6Ly9yZWRtaW5lLnRpbWUtaXNtb25leS5jb21pbnZlbnRvcnkzMTM=', 6, 'inventory', 313),
('aW52ZW50b3J5LmNvbWRlbnlzMzEz', 1, 'denys', 313),
('aXNNb25leS5jb21yb29tMzMxMw==', 1, 'room3', 313);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `state` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `display_name`, `password`, `state`) VALUES
(312, NULL, 'test@mail.ru', NULL, '$2y$14$Utl/pBbGDFRmcEFiaeGIauGfeuY7AW2RSQtyNcxm4hmCMlRt/27TK', NULL),
(313, NULL, NULL, 'id21748620', 'vkontakteToLocalUser', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_email`
--

CREATE TABLE `user_email` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_email`
--

INSERT INTO `user_email` (`user_id`, `email`, `id`) VALUES
(312, 'test@mail.ru', 13),
(312, 'Voodoo773@gmail.com', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `user_provider`
--

CREATE TABLE `user_provider` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider_id` varchar(50) NOT NULL,
  `provider` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_provider`
--

INSERT INTO `user_provider` (`user_id`, `provider_id`, `provider`) VALUES
(313, '21748620', 'vkontakte');

-- --------------------------------------------------------

--
-- Структура таблицы `user_registration`
--

CREATE TABLE `user_registration` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(16) NOT NULL,
  `request_time` datetime NOT NULL,
  `responded` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_registration`
--

INSERT INTO `user_registration` (`user_id`, `token`, `request_time`, `responded`) VALUES
(312, 'WaCRJZptZPsvi1es', '2017-05-10 09:18:20', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user_role`
--

INSERT INTO `user_role` (`id`, `role_id`, `is_default`, `parent_id`) VALUES
(1, 'guest', 0, NULL),
(2, 'user', 1, 'guest'),
(3, 'admin', 0, 'user'),
(4, 'group', 0, 'user'),
(5, 'delete', 0, 'guest'),
(6, 'user-partner', 0, 'user'),
(7, 'group-partner', 0, 'group');

-- --------------------------------------------------------

--
-- Структура таблицы `user_role_linker`
--

CREATE TABLE `user_role_linker` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user_role_linker`
--

INSERT INTO `user_role_linker` (`user_id`, `role_id`) VALUES
(312, 'user'),
(313, 'user');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Индексы таблицы `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Индексы таблицы `service_connect`
--
ALTER TABLE `service_connect`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_hash_UNIQUE` (`room_hash`);

--
-- Индексы таблицы `service_email`
--
ALTER TABLE `service_email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token_UNIQUE` (`token`),
  ADD KEY `fk_service_room_idx` (`hash`);

--
-- Индексы таблицы `service_image`
--
ALTER TABLE `service_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `service_room`
--
ALTER TABLE `service_room`
  ADD PRIMARY KEY (`service`,`service_room_name`,`user_id`),
  ADD UNIQUE KEY `hash_UNIQUE` (`hash`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `user_email`
--
ALTER TABLE `user_email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Индексы таблицы `user_provider`
--
ALTER TABLE `user_provider`
  ADD PRIMARY KEY (`provider_id`,`user_id`),
  ADD UNIQUE KEY `provider_id` (`provider_id`,`provider`),
  ADD KEY `user_provider_ibfk_1_idx` (`user_id`);

--
-- Индексы таблицы `user_registration`
--
ALTER TABLE `user_registration`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  ADD UNIQUE KEY `token_UNIQUE` (`token`);

--
-- Индексы таблицы `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_role` (`role_id`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Индексы таблицы `user_role_linker`
--
ALTER TABLE `user_role_linker`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `idx_role_id` (`role_id`),
  ADD KEY `idx_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT для таблицы `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `service_connect`
--
ALTER TABLE `service_connect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `service_email`
--
ALTER TABLE `service_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT для таблицы `service_image`
--
ALTER TABLE `service_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=314;
--
-- AUTO_INCREMENT для таблицы `user_email`
--
ALTER TABLE `user_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `user_registration`
--
ALTER TABLE `user_registration`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT для таблицы `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `service_email`
--
ALTER TABLE `service_email`
  ADD CONSTRAINT `fk_service_room` FOREIGN KEY (`hash`) REFERENCES `service_room` (`hash`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_email`
--
ALTER TABLE `user_email`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`email`) REFERENCES `email` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_provider`
--
ALTER TABLE `user_provider`
  ADD CONSTRAINT `fk_user_provider_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `user_role` (`role_id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `user_role_linker`
--
ALTER TABLE `user_role_linker`
  ADD CONSTRAINT `fk_user_role_linker_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role_linker_user_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
