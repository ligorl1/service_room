ZendSkeleton with registration and authorization of social networks
===================================================================

Introduction
------------
This is a simple, skeleton application using the ZF2 MVC layer and module
systems. This application is meant to be used as a starting place for those
looking to get their feet wet with ZF2.

Installation and settings
---------------------------
Import DB:
    
    /data/ScnSocialKeys.sql

Connection DB orm

    \module\Keys\config\module.config.php
    
For Module **ScnSocialKeys**:
    
    'connection' => array(
                'orm_default' => array(
                    'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                    'params' => array(
                        'host'     => '127.0.0.1',
                        'port'     => '3306',
                        'user'     => 'root',
                        'password' => '',
                        'dbname'   => 'auth',
                    )
                )
            )


Settings DB

    \config\autoload\db.local.php
    \config\autoload\profiler.local.php


