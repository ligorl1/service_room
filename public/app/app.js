angular
    .module('app', [],function($locationProvider){
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });


angular
    .module('app')
    .filter('offerFilter', offerFilter);

function offerFilter(){
    return function(input, term, isReserved, isExchange, params){
        if(!input || input.length == 0){
            return;
        }
        if(!term && !isReserved && !isExchange){
            return input;
        }
        var filtredItems = [];
        var params = params.split(',').map(function(item){ return item.trim()});
        filtredItems = input.filter(function (item) {
            var match = false;
            if((isReserved || isExchange) && !checkStatus(item, isReserved, isExchange)){
                return false;
            }
            // if((isExchange) && !checkStatusIsExchange(item)){
            //     return false;
            // }
            params.forEach(function(param){
                if(!item.hasOwnProperty(param)){
                   return;
                }
                if(!term || isMatch(item[param], term)){
                    match = true;
                    return;
                }
            });

            return match;
        });
        return filtredItems;
    };


    function isMatch(item, term, status){


        return item.toLowerCase().indexOf(term.toLowerCase()) !== -1;
    }

    function checkStatus(item, isReserved, isExchange){
        if( !item.hasOwnProperty('status')){
            return false;
        }
        return item.status.offer_type === 'reserve' && isReserved || item.status.offer_type === 'exchange' && isExchange;
    }

    function checkStatusIsReserved(item){
        if( !item.hasOwnProperty('status')){
            return false;
        }
        return item.status.offer_type === 'reserve';
    }

    function checkStatusIsExchange(item){
        if( !item.hasOwnProperty('status')){
            return false;
        }
        return item.status.offer_type === 'exchange';
    }
}