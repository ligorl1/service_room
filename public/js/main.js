// form style
function selectStyle() {
    $('.calculator select').selectmenu({
        select: function() {
            //$('#ModeSelection + .ui-selectmenu-button').removeClass('failed');
        },
        change: function() {
            if ($('#ModeSelection + .ui-selectmenu-button').hasClass('failed')) {
                $('#ModeSelection + .ui-selectmenu-button').removeClass('failed');
            }
            if ($('.calculator .tabs .tab-section.locations .ui-selectmenu-button').hasClass('failed')) {
                $('.calculator .tabs .tab-section.locations .ui-selectmenu-button').removeClass('failed');
            }
        }
    });
}

$(document).ready(function() {
    // javascript-overlay
    $('.calculator .overlay').hide();
    // radioChecked
    var shippingRadio = $('.calculator .shipping .shipping-list label');

    shippingRadio.find('input:checked').parents('label').addClass('active');
    shippingRadio.click(function() {
        shippingRadio.removeClass('active');
        $(this).find('input:checked').parents('label').addClass('active');
    });
    // form style
    selectStyle();
    $('.calculator input:radio').on('change', function() {
        selectStyle();
    });
    document.addEventListener('DOMContentLoaded', function(eve) {
        console.log('addEventListener')
    })
    document.onreadystatechange = function() {
        console.log('addEventListener')
    }
});

$(function() {
    $("#tabs").tabs();

    $("#sort").selectmenu();

    $("#pages").selectmenu();

    // $( "#origin-country" ).selectmenu();
    // $( "#origin-city" ).selectmenu();
    // $( "#destination-country" ).selectmenu();
    // $( "#destination-city" ).selectmenu();

});