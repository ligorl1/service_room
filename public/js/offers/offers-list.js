// offer-checkbox-block
$(document).ready(function() {
    var offerCheckboxLabel = $('.admin-wrapper .offer-checkbox-block .offer-checkbox-label');
    var offerCheckbox = $('.admin-wrapper .offer-checkbox-block input:checkbox');

    offerCheckbox.each(function(index, el) {
        if ($(el).is(":checked")) {
            $(el).next('label').toggleClass('btn-info btn-default');
        }
    });

    offerCheckbox.on('click', function(event) {
        $(this).next('label').toggleClass('btn-info btn-default');
    });
});
// END:offer-checkbox-block
