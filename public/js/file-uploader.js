$(function() {
	function fileUpload(event) {
		// Create a jQuery object from the form
		var files = event.target.files;



		//var formData = new FormData($('form[name="UserToCompanyForm"]'));
		var formData = new FormData();
		formData.append('logo', files['0']);

		// You should sterilise the file names
		// return false;
		$.ajax({
			url : '/' + lang + '/user/profile/' + user_id + '/uploadCompanyLogo',
			type : 'POST',
			data : formData,
			cache : false,
			dataType : 'json',
			processData: false,
			contentType: false,
			beforeSend: function(xhr){
				// console.log('progress', xhr);
			},
			xhr: function(){
				var xhr = $.ajaxSettings.xhr(); // получаем объект XMLHttpRequest
				xhr.upload.addEventListener('progress', function(evt){ // добавляем обработчик события progress (onprogress)
					if(evt.lengthComputable) { // если известно количество байт
						// высчитываем процент загруженного
						var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
						console.log('loaded', percentComplete);
						$('.progress-bar').html(percentComplete + '%').css('width', percentComplete + '%');

					}
				}, false);
				return xhr;
			},
			success : function(data, textStatus, jqXHR) {
				console.log('success',data);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				// Handle errors here
				console.log('ERRORS: ' + textStatus + ', ' +errorThrown);
			},
			complete : function(data) {
				// STOP LOADING SPINNER
				if(4 == data.readyState && data.status == 200){
					var reader = new FileReader();
					reader.onload = function(e){
						$('.companyLogo').attr('src', e.target.result);
						$('.upload-file').attr('style', 'background-image: url("'+e.target.result+'")');
					};
					reader.readAsDataURL(files['0']);
				}

				console.log('complete', data);
			}
		});
	}
	
	/*$('input[type="file"]').on('change', function(e){
		if(filesValidation(e.target.files)){
			$('.progress-bar').html( '0%').css('width',0);
			$('.progress').removeClass('hide');
			setTimeout(function(){
				fileUpload(e);
			},900);
		}else{
			e.value = '';
			alert('File invalid');
		}
	});*/

	function filesValidation(files){
		if(!files){
			return false;
		}
		var file = files[0];

		return file && file.type.match(/^image\//) && file.size <= 1024*1024;
	}
});