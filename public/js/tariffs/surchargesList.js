$(function(){
    $('body').on('click', '.confirm', function() {
      var c = confirm('Вы уверены, что хотите удалить этот тариф?');
      if(c == false){
        return false;
      }else{
    	  var url = $(this).attr('data-href');
    	  $.post(url, function(data){
    		  console.log(data);
    			if(data['status']){
        			if(data['id']){
        				/*удаление успешно*/
        				var tr = $("#"+data['id']);
        				var status = 'success';
        				var tbody = tr.parent();
        				var panel = tbody.parent().parent().parent();
        				tr.fadeOut(function(){
        					$(this).remove();
        					/* Если таблица пустая - удаляем её с панелью */
        					if(tbody.children().length === 0){
            					tbody.parent().fadeOut(function(){
                					$(this).remove();
            						if(panel.children().children().length <= 1){
                						panel.fadeOut();
            						}
                				});
            					var id = data['id'].split("_");
            					$('h4#'+id[0]).fadeOut(function(){
                					/* Если панель пустая - удаляем панель */
                					$(this).remove();
            						if(panel.children().children().length <= 1){
                						panel.fadeOut();
            						}
                				});
                		    }
        				});
        				
        			}
        			else if(data['type']){
        				var status = 'success';
        				var value = $('#'+data['type']).children()[1];
        				var valueMin = $('#'+data['type']).children()[2]
        				var remove = $('#'+data['type']).children()[3]
        				$(value).text('');
        				$(valueMin).text('-');
        				$(remove).text('')
        				$(value).append('<em>Тариф не указан</em>');
        			}
    			}else{
    				var status = 'danger';
    			}
    			/*выводим сообщение*/
    			var msg = data['message'];
    			var alert = '<div class="error">'+
                    			'<div class="alert alert-dismissible alert-'+status+'">'+
                        			'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                                     '×'+
                                 	'</button>'+
                                    '<ul class="notification-flash-list">'+
                                    	'<li>'+msg+'</li>'+
                                    '</ul>'+
                                '</div>'+
                            '</div>';
    			$(alert).appendTo("#notification").fadeIn();
    			
    		});
      	}
    });
});