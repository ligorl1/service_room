<?php
return [
    'bjyauthorize' => [
        
        // set the 'guest' role as default (must be defined in a role provider)
        'default_role'       => 'guest',
        // 'authenticated_role' => 'user',
        /*
         * this module uses a meta-role that inherits from any roles that should
         * be applied to the active user. the identity provider tells us which
         * roles the "identity role" should inherit from.
         *
         * for ZfcUser, this will be your default identity provider
         */
        'identity_provider'  => // 'BjyAuthorize\Provider\Identity\AuthenticationIdentityProvider',
            'BjyAuthorize\Provider\Identity\ZfcUserZendDb',
        
        /*
         * If you only have a default role and an authenticated role, you can
         * use the 'AuthenticationIdentityProvider' to allow/restrict access
         * with the guards based on the state 'logged in' and 'not logged in'.
         *
         * 'default_role' => 'guest', // not authenticated
         * 'authenticated_role' => 'user', // authenticated
         * 'identity_provider' => 'BjyAuthorize\Provider\Identity\AuthenticationIdentityProvider',
         */
        
        /*
         * role providers simply provide a list of roles that should be inserted
         * into the Zend\Acl instance. the module comes with two providers, one
         * to specify roles in a config file and one to load roles using a
         * Zend\Db adapter.
         */
        'role_providers'     => [
            
            /* here, 'guest' and 'user are defined as top-level roles, with
             * 'admin' inheriting from user
*/
            'BjyAuthorize\Provider\Role\Config' => [
                'guest' => [],
            ]
            // 'user' => array('children' => array(
            // 'admin' => array(),
            // )),
            ,
            
            // this will load roles from the user_role table in a database
            // format: user_role(role_id(varchar), parent(varchar))
            'BjyAuthorize\Provider\Role\ZendDb' => [
                'table'             => 'user_role',
                'role_id_field'     => 'role_id',
                'parent_role_field' => 'parent_id',
            ],
        ],
        // this will load roles from the 'BjyAuthorize\Provider\Role\Doctrine'
        // service
        // 'BjyAuthorize\Provider\Role\Doctrine' => array(),
        
        // resource providers provide a list of resources that will be tracked
        // in the ACL. like roles, they can be hierarchical
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'user_to_company' => [],
            ],
        ],
        // 'pants' => array(),
        
        /*
         * rules can be specified here with the format:
         * array(roles (array), resource, [privilege (array|string), assertion])
         * assertions will be loaded using the service manager and must implement
         * Zend\Acl\Assertion\AssertionInterface.
         * *if you use assertions, define them using the service manager!*
         */
        'rule_providers'     => [
            'BjyAuthorize\Provider\Rule\Config' => [
                'allow' => [
                    // // allow guests and users (and admins, through inheritance)
                    // // the "wear" privilege on the resource "pants"
                    // // array(array('guest', 'user'), 'pants', 'wear')
                    [
                        /* user to company менбю с выбором типа перевозки */
                        ['admin', 'user-partner', 'group-partner'],
                        'user_to_company',
                        ['company_transport_type'],
                    ],
                ],
                
                // Don't mix allow/deny rules if you are using role inheritance.
                // There are some weird bugs.
                'deny'  => [],
            ],
        ],
        
        /* Currently, only controller and route guards exist
         *
* Consider enabling either the controller or the route guard depending on your needs.
*/
        'guards'             => [
            'BjyAuthorize\Guard\Controller' => [
                [
                    'controller' => [
                        'Company\Controller\Index',
                    ] // Company\Controller\Index
                    ,
                    'roles'      => [
                        'admin',
                        'group',
                    ],
                ],
                [
                    'controller' => [
                        'Admin\Controller\Index',
                        'Admin\Controller\User',
                        'Admin\Controller\Company',
                        'Admin\Controller\Request',
                        'Admin\Controller\Profile',
                        'Admin\Controller\Api',
                        'Admin\Controller\Service',
                        'Admin\Controller\Tariffs',
                        'Admin\Controller\Warehouse',
                        'Admin\Controller\Carrier',
                        'Admin\Controller\FFService',
                    ],
                    'roles'      => [
                        'admin',
                    ],
                ],
                [
                    'controller' => [
                        'UserProfile\Controller\Index',
                        'UserProfile\Controller\Index',
                        'Company\Controller\Index',
                        'Api\Controller\Api',
                        'Company\Controller\Api',
                        'Acex\Controller\Api',
                        'Company\Controller\Index',
                    ],
                    'action'     => [
                        'index',
                        'profileEdit', // UserProfile\Controller\Index
                        'showProfilePhoto', // 'UserProfile\Controller\Index',
                        'deleteProfilePhoto',
                        'deleteProfileRequest',
                        'confirm',
                        'confirmEntry',
                        'rejectEntry',
                        'profile',
                        'getAllStatusEmployees', // 'Api\Comtroller\Api'
                        'request', // 'Api\Comtroller\Api'
                        'filter', // 'Api\Comtroller\Api'
                        'test', // 'Api\Comtroller\Api'
                        'getUserOffer',
                        //                         'uploadCompanyLogo', // 'UserProfileMod\Controller\Index'
                        //                         'upload',
                        'invite',//'Company\Controller\Index',
                    
                    ] // 'UserProfile\Controller\Index',
                    ,
                    'roles'      => [
                        'user',
                    ],
                ],
                [
                    'controller' => [
                        'UserProfile\Controller\Api',
                        'zfcuser',
                        'SanSessionToolbar\Controller\SessionToolbar',
                    ],
                    'roles'      => [
                        'user',
                    ],
                ],
                [
                    'controller' => ['Acex\Controller\Admin'],
                    'roles'      => ['guest'],
                ],
                [
                    'controller' => [
                        'Application\Controller\Index',
                        'zfcuser',
                        'Voodoo773Localization\Controller\Index',
                        'Acex\Controller\Service',
                        'Company\Controller\Index',
                        'HtUserRegistration',
                        'AcexMod\Controller\Service',
                        'goalioforgotpassword_forgot',
                    ],
                    'action'     => [
                        'index', // 'Application\Controller\Index',
                        'login',
                        'register',
                        'provider-login',
                        'provider-authenticate',
                        'convert', // 'Acex\Controller\Service',
                        'getCity',
                        'removeUser',
                        'verify-email', //'HtUserRegistration'
                        'addUserOffer', // AcexMod\Controller\Service
                        'companyProfile', //'Company\Controller\Index',
                        'forgot',   //goalioforgotpassword_forgot
                        'reset',    //goalioforgotpassword_forgot
                        'getOfferList',
                        'upload',
                    ] // Acex\Controller\Api
                    
                    , // UserProfile\Controller\Index
                    'roles'      => [
                        'guest',
                    ],
                ],
                [
                    'controller' => ['Admin\Controller\FFService'],
                    'action'     => ['index'],
                    'roles'      => ['guest'],
                ],
                [
                    'controller' => ['Admin\Controller\Index'],
                    'action'     => ['console'],
                    'roles'      => ['guest'],
                ],
                [
                    'controller' => ['Voodoo773Localization\Controller\Index'],
                    'action'     => [
                        'contact',
                        'careers',
                        'resources',
                        'about',
                        'lists',
                        'privacy',
                        'signIn',
                        'becomePartner',
                        'startUsing',
                    ],
                    'roles'      => ['guest'],
                ],
                [
                    'controller' => [
                        'Acex\Controller\Service',
                        'UserProfile\Controller\Index',
                    ],
                    'roles'      => ['guest'],
                ],
                [
                    'controller' => 'Company\Controller\Index',
                    'roles'      => [
                        'group',
                        'admin',
                    ],
                ],
                [
                    'controller' => 'Exchange\Controller\Index',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'Exchange\Controller\Api',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'Exchange\Controller\UserExchange',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'Exchange\Controller\CompanyExchange',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'Mailer\Controller\Gearman',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'CompanyUserAccess\Controller\CompanyRole',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'CompanyUserAccess\Controller\CompanyUserRole',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'Company\Controller\CompanyMail',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'CompanyAir\Controller\Tariff',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'CompanyAir\Controller\Cabinet',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'CompanyAir\Controller\Charge',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'CompanyAir\Controller\Api',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'Acex\Controller\Api',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'UserProfileMod\Controller\Index',
                    'roles'      => 'user',
                ],
                [
                    'controller' => 'ScnSocialAuth-HybridAuth',
                    'roles'      => 'guest',
                ],
                [
                    'controller' => 'ScnSocialAuth-User',
                    'roles'      => 'guest',
                ],
            ],
            /* If this guard is specified here (i.e. it is enabled), it will block
             * access to all controllers and actions unless they are specified here.
* You may omit the 'action' index to allow access to the entire controller
*/
            //             'BjyAuthorize\Guard\Controller' => array(
            //             ),
            
            /* If this guard is specified here (i.e. it is enabled), it will block
             * access to all routes unless they are specified here.
*/
            //             'BjyAuthorize\Guard\Route' => array(
            // //                 array('route' => 'zfcuser', 'roles' => array('user')),
            // //                 array('route' => 'zfcuser/logout', 'roles' => array('user')),
            // //                 array('route' => 'zfcuser/login', 'roles' => array('guest')),
            // //                 array('route' => 'zfcuser/register', 'roles' => array('guest')),
            // //                 // Below is the default index action used by the ZendSkeletonApplication
            // //                 array('route' => 'home', 'roles' => array('guest', 'user')),
            //             ),
        ],
    ],
];
