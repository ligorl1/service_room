<?php
return [
    'navigation' => [
        'myNav'     => [
            [
                'label' => 'Вход',
                'route' => 'localization/sign-in',
            ],
            [
                'label' => 'Биржа',
                'route' => 'localization/exchange',
            ],
            [
                'label' => 'Для пользователей',
                'route' => 'localization/start-using',
            ],
            [
                'label' => 'Для партнеров',
                'route' => 'localization/become-partner',
            ],
        ],
        'resources' => [
            [
                'label' => 'Порты',
                'route' => 'localization',
            ],
            [
                'label' => 'Ресурсы и инструменты',
                'route' => 'localization/resources',
            ],
            [
                'label' => 'Ведомость индустрии',
                'route' => 'localization/lists',
            ],
        ],
        'acex'      => [
            [
                'label' => 'Биржа',
                'route' => 'localization/exchange',
            ],
            [
                'label' => 'Контакт',
                'route' => 'localization/contact',
            ],
            [
                'label' => 'Карьера',
                'route' => 'localization/careers',
            ],
            [
                'label' => 'Условия',
                'route' => 'localization/privacy',
            ],
        ],
        'language'  => [
            [
                'label'  => 'Русский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'ru',
                ],
            ],
            [
                'label'  => 'Английский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'en',
                ],
            ],
            [
                'label'  => 'Немецкий',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'de',
                ],
            ],
            [
                'label'  => 'Французский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'fr',
                ],
            ],
            [
                'label'  => 'Португальский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'pt',
                ],
            ],
            [
                'label'  => 'Итальянский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'it',
                ],
            ],
            [
                'label'  => 'Испанский',
                'route'  => 'localization/careers',
                'params' => [
                    'lang' => 'es',
                ],
            ],
        ],
    ],
];
