<?php

return [
    
    'mt_mail' => [
        
        /*
         * Transport settings
         */
        
        'transport'         => 'Zend\Mail\Transport\Smtp',
        'transport_options' => [
            'name'              => 'Board',
            'host'              => 'mail.ukraine.com.ua',
            'port'              => 25,
            'connection_class'  => 'plain',
            'connection_config' => [
                'username' => 'noreply@time-ismoney.com',
                'password' => '4KxNt2e1',
            ],
        ],
        
        //         'transport' => 'Zend\Mail\Transport\Smtp',
        //         'transport_options' => array(
        //             "name" => "Board",//Board
        //             'host' => 'debugmail.io',
        //             "port" => 25,
        //             'connection_class' => 'plain',
        //             'connection_config' => array(
        //                 'username' => 'voodoo773@gmail.com',
        //                 'password' => 'e3109ef0-02f1-11e6-acb8-b387215ae1ba',
        //             ),
        //         ),
        
        /*
         * List of enabled composer plugins
         * Uncomment name of plugin you want to enable, then uncomment and edit its options below
         */
        'composer_plugins'  => [
            // 'Layout',
            'MessageEncoding',
        ],
        
        /*
         * Plugin configuration
         */
        
        // default header example
        /*'default_headers' => array(
            'From' => 'information-no-reply@yourwebsite.com',
            'Reply-To' => 'Website Admin <admin@yourwebsite.com>',
        ),*/
        
        // message layout - path to view script
        // 'layout' => 'application/mail/layout.phtml',
        
        // example message encoding
        'message_encoding'  => 'UTF-8',
    ],

];
