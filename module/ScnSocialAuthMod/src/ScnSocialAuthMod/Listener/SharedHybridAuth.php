<?php
namespace ScnSocialAuthMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class SharedHybridAuth implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    protected $listeners = [];
    
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        /*
         * register.pre
         * register.post
         * */
        $this->listeners[] = $events->attach(
            'ScnSocialAuth\Authentication\Adapter\HybridAuth',
            'register.pre',
            [$this, 'registerPre'],
            100
        );
        
        $this->listeners[] = $events->attach(
            'ScnSocialAuth\Authentication\Adapter\HybridAuth',
            'register.post',
            [$this, 'registerPost'],
            100
        );
        
        
        /*
         * registerViaProvider
         * registerViaProvider.post
         * 
         * */
        $this->listeners[] = $events->attach(
            'ScnSocialAuth\Authentication\Adapter\HybridAuth',
            'registerViaProvider',
            [$this, 'registerViaProvider'],
            100
        );
        
        $this->listeners[] = $events->attach(
            'ScnSocialAuth\Authentication\Adapter\HybridAuth',
            'registerViaProvider.post',
            [$this, 'registerViaProviderPost'],
            100
        );
        
        /*
         * scnUpdateUser.pre
         * scnUpdateUser.post
         * */
        $this->listeners[] = $events->attach(
            'ScnSocialAuth\Authentication\Adapter\HybridAuth',
            'scnUpdateUser.pre',
            [$this, 'scnUpdateUserPre'],
            100
        );
    }
    
    /*
     * */
    public function registerPre(EventInterface $e)
    {
        echo 'registerPre <hr/>';
    }
    
    /*
     * add Role to table
     * */
    public function registerPost(EventInterface $e)
    {
        $ServiceLocator = $this->getServiceLocator();
        //получаем User для проверки
        $UserEntity = $e->getParam('user');
        
        //Узнать есть ли USER_ROLE_LINKER
        $UserRoleLinkerMapper = $this->getServiceLocator()->get('BjyAuthorizeMod\Mapper\UserRoleLinker');
        $UserRoleLinkerEntity = $UserRoleLinkerMapper->getEntityListByFilter([
            'user_id' => $UserEntity->getId(),
        ])->current();
        
        if (!empty($UserRoleLinkerEntity)) {
            return;
        }
        
        /*
         * Получаем роль по умолчанию из 
         */
        $UserRoleMapper = $ServiceLocator->get('BjyAuthorizeMod\Mapper\UserRole');
        /**
         * @var \BjyAuthorizeMod\Entity\UserRole $UserRoleEntity
         */
        $UserRoleEntity = $UserRoleMapper->getEntityListByFilter([
            'is_default' => 1,
        ])->current();
        
        //var_dump($UserRoleEntity);
        
        /*
         * Установить роль по умолчнаию для зарегистрированных пользователей
         */
        $UserRoleLinkerMapper = $ServiceLocator->get('BjyAuthorizeMod\Mapper\UserRoleLinker');
        $UserRoleLinkerEntity = $UserRoleLinkerMapper->getEntityPrototype();
        //Установить значение ID того email который есть в системе и USER_ROLE по умолчанию
        $UserRoleLinkerEntity->setUserId($UserEntity->getId());
        //Изменили ID роли
        $UserRoleLinkerEntity->setRoleId($UserRoleEntity->getRoleId());
        
        $ResultInsert = $UserRoleLinkerMapper->insert($UserRoleLinkerEntity);
    }
    
    /*
     * */
    public function registerViaProvider(EventInterface $e)
    {
        echo 'registerViaProvider <hr/>';
    }
    
    /*
     * */
    public function scnUpdateUserPre(EventInterface $e)
    {
    }
    
    public function registerViaProviderPost(EventInterface $e)
    {
        echo 'registerViaProviderPost <hr/>';
    }
    
}
