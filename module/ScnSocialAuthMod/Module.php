<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ScnSocialAuth for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ScnSocialAuthMod;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Debug\Debug;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    function getServiceConfig() {
        return [
            'invokables' => [
                'ScnSocialAuthMod\Listener\SharedHybridAuth'
                    => 'ScnSocialAuthMod\Listener\SharedHybridAuth',
            ],
            'initializers' => [
                function ($service, ServiceLocatorInterface $SL) {
                    if ($service instanceof ServiceLocatorAwareInterface) {
                        $service->setServiceLocator($SL);
                    }
                }
            ]
        ];
    }
    
    
    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $SEM = $eventManager->getSharedManager();
        $ServiceLocator = $e->getApplication()->getServiceManager();
//         Debug::dump($ServiceLocator->get('Config')['view_manager']);
        $SEM->attachAggregate( $ServiceLocator->get('ScnSocialAuthMod\Listener\SharedHybridAuth'));
    }
}
