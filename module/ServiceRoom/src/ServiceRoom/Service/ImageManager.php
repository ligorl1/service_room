<?php
namespace ServiceRoom\Service;


class ImageManager
{


    private $saveToDir = './data/service/';
    public function getSaveToDir()
    {
        return $this->saveToDir;
    }
    public function setSaveToDir($saveToDir)
    {
        $this->saveToDir = $saveToDir;
    }

    public function getSavedFiles()
    {
        // Каталог, куда мы планируем сохранять выгруженные файлы..

        // Проверяем, существует ли уже каталог, и, если нет, то
        // создаем его.
        if(!is_dir($this->saveToDir)) {
            if(!mkdir($this->saveToDir)) {
                throw new \Exception('Could not create directory for uploads: ' .
                    error_get_last());
            }
        }

        // Просматриваем каталог и создаем список выгруженных файлов.
        $files = [];
        $handle  = opendir($this->saveToDir);
        while (false !== ($entry = readdir($handle))) {

            if($entry=='.' || $entry=='..')
                continue; // Пропускаем текущий и родительский каталоги.

            $files[] = $entry;
        }

        // Возвращаем список выгруженных файлов.
        return $files;
    }

    // Возвращает путь к сохраненному файлу изображения.
    public function getImagePathByName($fileName)
    {
        // Принимаем меры предосторожности, чтобы сделать файл безопасным.
        $fileName = str_replace("/", "", $fileName);  // Убираем слеши.
        $fileName = str_replace("\\", "", $fileName); // Убираем обратные слеши.

        // Возвращаем сцепленные имя каталога и имя файла.
        return $this->saveToDir . $fileName;
    }

    public function deleteImagePathByName($fileName)
    {
        // Принимаем меры предосторожности, чтобы сделать файл безопасным.
        $fileName = str_replace("/", "", $fileName);  // Убираем слеши.
        $fileName = str_replace("\\", "", $fileName); // Убираем обратные слеши.

        // Возвращаем сцепленные имя каталога и имя файла.
        return unlink($this->saveToDir . $fileName);
    }

    // Возвращает содержимое файла изображения. При ошибке возвращает булевое false.
    public function getImageFileContent($filePath)
    {
        return file_get_contents($filePath);
    }

    // Извлекает информацию о файле (размер, MIME-тип) по его пути.
    public function getImageFileInfo($filePath)
    {
        // Пробуем открыть файл
        if (!is_readable($filePath)) {
            return false;
        }

        // Получаем размер файла в байтах.
        $fileSize = filesize($filePath);

        // Получаем MIME-тип файла.
        $finfo = finfo_open(FILEINFO_MIME);
        $mimeType = finfo_file($finfo, $filePath);
        if($mimeType===false)
            $mimeType = 'application/octet-stream';

        return [
            'size' => $fileSize,
            'type' => $mimeType
        ];
    }

    public  function resizeImage($filePath, $desiredWidth = 240)
    {
        // Получаем исходную размерность файла.
        list($originalWidth, $originalHeight) = getimagesize($filePath);

        // Вычисляем соотношение сторон.
        $aspectRatio = $originalWidth/$originalHeight;
        // Вычисляем получившуюся высоту.
        $desiredHeight = $desiredWidth/$aspectRatio;

        // Получаем информацию об изображении
        $fileInfo = $this->getImageFileInfo($filePath);

        // Изменяем размер изображения.
        $resultingImage = imagecreatetruecolor($desiredWidth, $desiredHeight);
        if (substr($fileInfo['type'], 0, 9) =='image/png')
            $originalImage = imagecreatefrompng($filePath);
        else
            $originalImage = imagecreatefromjpeg($filePath);
        imagecopyresampled($resultingImage, $originalImage, 0, 0, 0, 0,
            $desiredWidth, $desiredHeight, $originalWidth, $originalHeight);

        // Сохраняем измененное изображение во временное хранилище.
        $tmpFileName = tempnam("/tmp", "FOO");
        imagejpeg($resultingImage, $tmpFileName, 80);

        // Возвращаем путь к получившемуся изображению.
        return $tmpFileName;
    }

    public  function saveResizeImage($path,$toFilePath)
    {
        $filePath = $path.$toFilePath;
        // Получаем исходную размерность файла.
        list($originalWidth, $originalHeight) = getimagesize($filePath);
        $desiredWidth1 = 25;
        $desiredWidth2 = 100;
        $desiredWidth3 = 300;
        // Вычисляем соотношение сторон.
        $aspectRatio = $originalWidth/$originalHeight;
        // Вычисляем получившуюся высоту.
        $desiredHeight1 = $desiredWidth1/$aspectRatio;
        $desiredHeight2 = $desiredWidth2/$aspectRatio;
        $desiredHeight3 = $desiredWidth3/$aspectRatio;

        if($originalWidth > $desiredWidth1){
            $resultingImage1 = $this->getResultImage($filePath,$desiredWidth1,$desiredHeight1,$originalWidth,$originalHeight);
            imagejpeg($resultingImage1, $path.$desiredWidth1."x".$desiredWidth1."_".$toFilePath);
        }else{
            copy($filePath, $path.$desiredWidth1."x".$desiredWidth1."_".$toFilePath);
        }

        if($originalWidth > $desiredWidth2){
            $resultingImage1 = $this->getResultImage($filePath,$desiredWidth2,$desiredHeight2,$originalWidth,$originalHeight);
            imagejpeg($resultingImage1, $path.$desiredWidth2."x".$desiredWidth2."_".$toFilePath);
        }else{
            copy($filePath, $path.$desiredWidth2."x".$desiredWidth2."_".$toFilePath);
        }
        if($originalWidth > $desiredWidth3){
            $resultingImage1 = $this->getResultImage($filePath,$desiredWidth3,$desiredHeight3,$originalWidth,$originalHeight);
            imagejpeg($resultingImage1, $path.$desiredWidth3."x".$desiredWidth3."_".$toFilePath);
        }else{
            copy($filePath, $path.$desiredWidth3."x".$desiredWidth3."_".$toFilePath);
        }


        // Возвращаем путь к получившемуся изображению.
        return $toFilePath;
    }

    public  function saveFile($path)
    {
        $filePath = $path;






        return $path;
    }

    private function getResultImage($filePath,$desiredWidth,$desiredHeight,$originalWidth,$originalHeight){
        // Получаем информацию об изображении
        $fileInfo = $this->getImageFileInfo($filePath);

        // Изменяем размер изображения.
        $resultingImage = imagecreatetruecolor($desiredWidth, $desiredHeight);
        if (substr($fileInfo['type'], 0, 9) =='image/png')
            $originalImage = imagecreatefrompng($filePath);
        else
            $originalImage = imagecreatefromjpeg($filePath);
        imagecopyresampled($resultingImage, $originalImage, 0, 0, 0, 0,
            $desiredWidth, $desiredHeight, $originalWidth, $originalHeight);
        return $resultingImage;
    }

}