<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ServiceRoom\Controller;


use GuzzleHttp\Client;
use ScnSocialKeys\Entity\Email;
use ScnSocialKeys\Entity\UserEmail;
use ServiceRoom\Entity\Connect;
use ServiceRoom\Entity\Room;
use ServiceRoom\Entity\Service;
use ServiceRoom\Entity\ServiceEmail;
use ServiceRoom\Entity\ServiceImage;
use ServiceRoom\Entity\User;
use ServiceRoom\Form\ConnectRedmineForm;
use ServiceRoom\Form\EditRoomForm;
use ServiceRoom\Form\EditServiceForm;
use ServiceRoom\Service\ImageManager;
use Zend\Debug\Debug;
use Zend\Filter\File\RenameUpload;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\Date;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Redmine;

class RoomController extends AbstractActionController
{


    public function indexAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Room';
        $data['service'] = [];


        $serviceSingle = $em->getRepository(Room::class)->findBy(['roomUser' => $userId]);
        $roomHash = $this->params()->fromRoute('hash', 0);

        foreach ($serviceSingle as $value) {

            $flagActive[$value->getRoomService()->getName()][] = ($roomHash === $value->getHash()) ? true : false;
            $mas[$value->getRoomService()->getName()][] = [
                'nameRoom' => $value->getServiceName(),
                'hash' => $value->getHash(),
                'active' => ($roomHash === $value->getHash()) ? true : false

            ];
            $data['service']['list'][$value->getRoomService()->getName()] = [
                'room' => $mas[$value->getRoomService()->getName()],
                'active' => (array_search(true, $flagActive[$value->getRoomService()->getName()]) !== false) ? true : false,
                'serviceId' => $value->getRoomService()->getId()
            ];
            //Debug::dump($value->getRoomService()->getId());

        }

        //Debug::dump($data['service']['list']);


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }


    public function editAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Room Edit';
        $roomHash = $this->params()->fromRoute('hash', 0);
        $data['service'] = [];


        $formEditRoom = new EditRoomForm();

        $room = $em->getRepository(Room::class)->findOneBy(['hash' => $roomHash]);
        $service = $em->getRepository(Service::class)->findService();
        if (!empty($service)) {
            $formService = $formEditRoom->get('service');
            $formService->setValueOptions($service);
            $formService->setValue($room->getRoomService());
        }
        $formName = $formEditRoom->get('name');
        $formName->setValue($room->getServiceName());

        if ($this->getRequest()->isPost()) {
            $dataForm = $this->params()->fromPost();
            $formEditRoom->setData($dataForm);
            if ($formEditRoom->isValid()) {
                $dataForm = $formEditRoom->getData();

                $serviceObject = $em->getRepository(Service::class)->findOneBy(['id' => $dataForm['service']]);
                $userObject = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
                try {

                    $room->setServiceName($dataForm['name']);
                    $room->setRoomService($serviceObject);
                    $em->persist($room);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage("Создан успешно");
                    return $this->redirect()->toRoute('service-room/item', ['hash' => $roomHash]);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            }
        }
        $data['service']['form'] = $formEditRoom;


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function newAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Room New';

        $data['service'] = [];


        $formEditRoom = new EditRoomForm();

        $service = $em->getRepository(Service::class)->findService();
        if (!empty($service)) {
            $formService = $formEditRoom->get('service');
            $formService->setValueOptions($service);
        }

        if ($this->getRequest()->isPost()) {
            $dataForm = $this->params()->fromPost();
            $formEditRoom->setData($dataForm);
            if ($formEditRoom->isValid()) {
                $dataForm = $formEditRoom->getData();

                $serviceObject = $em->getRepository(Service::class)->findOneBy(['id' => $dataForm['service']]);
                $userObject = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
                try {
                    $roomSingle = new Room();
                    $roomSingle->setServiceName($dataForm['name']);
                    $roomSingle->setRoomService($serviceObject);
                    $roomSingle->setRoomUser($userObject);

                    $hash = base64_encode($serviceObject->getName() . $dataForm['name'] . $userId);
                    $roomSingle->setHash($hash);
                    $em->persist($roomSingle);
                    $em->flush();
                    $this->flashMessenger()->addSuccessMessage("Создан успешно");
                    return $this->redirect()->toRoute('service-room/item', ['hash' => $hash]);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            }
        }
        $data['service']['form'] = $formEditRoom;


        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function deleteAction()
    {

        $this->flashMessenger()->clearCurrentMessages();

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $roomHash = $this->params()->fromRoute('hash', 0);
        if ($roomHash) {
            $roomSingle = $em->getRepository(Room::class)->findOneBy(['hash' => $roomHash]);
            $em->remove($roomSingle);
            $em->flush();
            $this->flashMessenger()->addSuccessMessage("Удален успешно");
        }

        return $this->redirect()->toRoute('service-room');
    }

    private function getUserId($route = null)
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            //redirct $route
        }
        return $this->zfcUserAuthentication()->getIdentity()->getId();
    }


    public function connectAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Connect';
        $roomHash = $this->params()->fromRoute('hash', 0);
        //$room = $em->getRepository(Room::class)->findOneBy(['hash' => $roomHash]);
        $data['hash'] = $roomHash;
        /*Вытащить если есть apikey*/
        $connectRedmine = $em->getRepository(Connect::class)->findOneBy(['connectUser' => $userId, 'hash' => $roomHash]);
        $flagConnectRedmine = false;
        if ($connectRedmine) {
            $url = $connectRedmine->getUrl();
            $apiKey = $this->connectRedmine($url,$connectRedmine->getKey());//'https://redmine.time-ismoney.com',0,"baksspro","Rjcjhjnjd44"
            if($apiKey) {
                $flagConnectRedmine = true;
            }
        }
        if ($flagConnectRedmine) {
            $data['service']['redmine'] = true;
        } else {
            $formConnectRedmine = new ConnectRedmineForm();
            if ($this->getRequest()->isPost()) {
                $dataForm = $this->params()->fromPost();

                if ("redmine" == $dataForm['connect']) {
                    $formConnectRedmine->setData($dataForm);
                    if ($formConnectRedmine->isValid()) {
                        $dataForm = $formConnectRedmine->getData();
                        $userObject = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
                        try {
                            if (!$connectRedmine) {
                                $connectRedmine = new Connect();
                            }
                            //$flagConnectRedmine = true;
                            $apiKey = $this->connectRedmine($dataForm['url'], 0, $dataForm['login'], $dataForm['password']);
                            if (!$apiKey) {
                                $this->flashMessenger()->addErrorMessage("Подключение не удалось");
                            } else {
                                $connectRedmine->setConnectUser($userObject);
                                $connectRedmine->setHash($roomHash);
                                $connectRedmine->setConnect($dataForm['connect']);
                                $connectRedmine->setKey($apiKey);
                                $connectRedmine->setUrl($dataForm['url']);

                                $em->persist($connectRedmine);
                                $em->flush();

                                $this->flashMessenger()->addSuccessMessage("Подключение успешно");
                                return $this->redirect()->toRoute('service-room/item/connect', ['hash' => $roomHash]);
                            }
//
                        } catch (\Exception $e) {
                            $this->flashMessenger()->addErrorMessage($e->getMessage());
                        }
                    }
                }
            }

            $data['service']['form'] = $formConnectRedmine;
        }
        //Debug::dump($data['service']['list']);

        $data['email']['all'] = $this->getAllEmail($roomHash,date("Y-m-d"));

        //return new JsonModel(['data'=>$data]);
        return new ViewModel(['data' => $data]);
    }

    public function imageAction()
    {
        $userId = $this->getUserId();
        $this->flashMessenger()->clearCurrentMessages();
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data['title'] = 'Image';
        $roomHash = $this->params()->fromRoute('hash', 0);

        $emailId = $this->params()->fromRoute('emailId', 0);

        $dateImage = $this->params()->fromRoute('dateImage', 0);
        $data['dateTitle'] = $dateImage;
        $validator = new Date(array('format' => 'Y-m-d'));
        $data['image'] =[];
        if($validator->isValid($dateImage)){
//            Debug::dump( new \DateTime($dateImage));
            $data['image'] = $em->getRepository(ServiceImage::class)->findDate($dateImage); //$this->getAllEmailUser($roomHash, $emailId,$dateImage);

            $datePath = explode("-",$dateImage);

            $uri = $this->getRequest()->getUri();
            $base = sprintf('%s://%s', $uri->getScheme(), $uri->getHost());
            $data['src'] = $base."/service/$roomHash/$emailId"."/".$datePath[0]."/".$datePath[1]."/".$datePath[2];

        }
        return new ViewModel(['data' => $data]);
    }

    public function emailjsonAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $userId = $this->getUserId();
        $userObject = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
        $roomHash = $this->params()->fromPost('hash', 0);
        $data['email'] = [];


        $connectObject = $em->getRepository(Connect::class)->findOneBy(['hash' => $roomHash,'connectUser'=>$userId]);
        if($connectObject){
            $client = new Redmine\Client($connectObject->getUrl(),$connectObject->getKey());
            $users = $client->user->all();

            foreach ($users['users'] as $user){
                $flag_import = false;

                $serviceEmail = $em->getRepository(ServiceEmail::class)->findOneBy(['email' => $user['mail'],'hash' => $roomHash]);
                if(!$serviceEmail){
                    $serviceEmail = new ServiceEmail();
                    $serviceEmail->setHash($roomHash);
                    $serviceEmail->setCreateDate(new \DateTime(date("Y-m-d H:i:s")));
                    $serviceEmail->setConnectUser($userObject);
                    $serviceEmail->setEmail($user['mail']);
                    $serviceEmail->setToken($this->getToken($roomHash,$user['mail']));
                    $em->persist($serviceEmail);
                    $flag_import = true;


                }
                $email = $em->getRepository(Email::class)->findOneBy(['email' => $user['mail']]);
                if(!$email){
                    $email = new Email();
                    $email->setEmail($user['mail']);
                    $em->persist($email);
                    $flag_import = true;
                }
                $em->flush();

                $data['email'][] = [
                    'email' => $user['mail'],
                    'import' => $flag_import
                ];
            }
        }
        $data['all'] = $this->getAllEmail($roomHash,date("Y-m-d"));

        return new JsonModel($data);
    }

    function setFileJsonAction(){
        $data=[
            'answer' => "err",
            'message' => ["Не корректный запрос"],
            'data' => []
        ];



        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest();
            $result = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
//            $result=[];
//            $symbolRequest = $this->getRequest()->getContent();
//
//            if (!empty($symbolRequest)) {
//                $symbolRequest = explode(" ", $symbolRequest);
//
//                foreach ($symbolRequest as $symbolR) {
//                    $symbol = explode("\n", $symbolR);
//                    if(count($symbol) > 4){
//                        $r = explode("=",  $symbol[0]);
//                        $result[trim(trim($r[1]),"\"")] = trim(trim($symbol[2]),"\"");
//                    }
//
//                }
//            }
            //$data['data']['result']=$this->getRequest()->getContent();
            if(isset($result['hash']))
            {
                $hash = $result['hash'];
                //$data['data']['hash']=$hash;
            }else{
                $data['data']['hash']='hash';
                return new JsonModel($data);
            }
            if(isset($result['token']))
            {
                $token = $result['token'];
                //$data['data']['token']=$token;
            }else{
                $data['data']['token']='token';
                return new JsonModel($data);
            }
            if(isset($result['timestamp']))
            {
                $timestamp = $result['timestamp'];
            }else{
                $data['data']['timestamp']='timestamp';
                return new JsonModel($data);
            }
            if(isset($result['file_name']))
            {
                $fileName = $result['file_name'];
            }else{
                $data['data']['file_name']='file_name';
                return new JsonModel($data);
            }

            $userEmailObject = $em->getRepository(ServiceEmail::class)->findOneBy(['hash' => $hash,'token'=>$token]);
            if(!empty($userEmailObject)){
                $email = $userEmailObject->getEmail();
                $userId = 0;

                $userObject = $em->getRepository(UserEmail::class)->findOneBy(['email' => $email]);
                if(!empty($userObject)){
                    $userId = $userObject->getUserId();
                }
                $path = $this->getPathFile($result['hash'],$email."_".$userId);
                //$data['data']['path']=$path;
                //-------сохраняем файл
                $dateCreate = date("Y-m-d H:i:s",$timestamp/1000);
                $imageData = $result['file'];
                $imageData = str_replace('data:image/jpeg;base64,', '', $imageData);
                $imageData = str_replace(' ', '+', $imageData);
                $imageData = base64_decode($imageData);
                $nameFile = $fileName."_".date("Y_m_d_H_i_s",$timestamp/1000).".jpg";
                $success = file_put_contents("$path/$nameFile", $imageData);


                $source_img = imagecreatefromstring($imageData);

                list($width, $height) = getimagesizefromstring($imageData);
                $ratio = $width/$height;
                $newwidth = 200;
                $newheight = $newwidth/$ratio;
                $source_img_result=imagecreatetruecolor($newwidth, $newheight);;
                //imagecopyresized($source_img_result,$source_img,0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                imagecopyresampled($source_img_result,$source_img,0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                $imageSave = imagejpeg($source_img_result, "$path/$newwidth"."_$nameFile");
                imagedestroy($source_img);
                imagedestroy($source_img_result);

                if($success && $imageSave){
                    $serviceImage = new ServiceImage();
                    $serviceImage->setImage($nameFile);
                    $serviceImage->setServiceEmailId($userEmailObject);
                    $serviceImage->setCreateDate(new \DateTime($dateCreate));
                    $em->persist($serviceImage);
                    $em->flush();
                    $data['answer'] = "ok";
                    $data['message'] ="Файл сахранен";
                }else{
                    $data['data']['file']='file';
                    return new JsonModel($data);
                }


//                $nameDir = basename($result['file']['name']);
//                $uploaded = move_uploaded_file( $result['file']['tmp_name'] , "$path/$nameDir");
//                if($uploaded){
//                    $data['answer'] = "ok";
//                    $data['message'] ="Файл сахранен";
//                }else{
//                    $data['data']='file';
//                    return new JsonModel($data);
//                }
            }
        }

        return new JsonModel($data);
    }

    function testJsonAction(){
        $data=[];
        $lang = $roomHash = $this->params()->fromRoute('lang', 'ru');
        $url =  $this->url()->fromRoute("json/test",['lang'=>$lang]);

        $formObject = new EditServiceForm();
        $form = [
            'form'=>[
                'metod' => "POST",
                'action' => $url
            ],
            'input'=>[
                0 =>[
                    'name' => "name",
                    'type' => "text",
                    'value' => "",
                    'label' => [
                        'value' => "label test"
                    ]
                ],
                1 =>[
                    'name' => "edit",
                    'type' => "submit",
                    'value' => "Сохранить",
                ]
            ],
            'valid' => null,
            'message' => []
        ];

        if ($this->getRequest()->isPost()) {
            $dataForm = $this->params()->fromPost();
            $formObject->setData($dataForm);
            if($dataForm['flag']){
                if ($formObject->isValid()) {

                }
                    $errors = $formObject->getMessages();
                    $form['valid'] = count($errors);
                    $form['message'] = $errors;

            }

        }



        return new JsonModel($form);

    }
    function testJson2Action()
    {
        $data = [];
        $lang = $roomHash = $this->params()->fromRoute('lang', 'ru');


        return;
    }

//    function getAllEmailUser($hash, $emailId, $datePath){
//
//        $result =[];
//        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
//        $emailUser = explode("_",$emailId);
//
//        if($emailUser[1]){
//            $userEmail = $em->getRepository(UserEmail::class)->findOneBy(['email' => $emailUser[0], 'userId'=>$emailUser[1]]);
//
//            if($userEmail){
//
//                $serviceEmail = $em->getRepository(ServiceEmail::class)->findOneBy(['hash' => $hash, 'email'=>$emailUser[0]]);
//
//            }else{
//                return false;
//            }
//        }else{
//            $serviceEmail = $em->getRepository(ServiceEmail::class)->findOneBy(['hash' => $hash, 'email'=>$emailUser[0]]);
//        }
//
//
//        $path = "./public/service/$hash";
//        $datePath = explode("-",$datePath);
//            $dirPath = $emailId."/".$datePath[0]."/".$datePath[1]."/".$datePath[2];
//            $dir = "$path/".$dirPath;
//
//            if(file_exists($dir)){
//                $result = scandir($dir);
//            }
//
//
//        return $result;
//    }
    //date("Y-m-d")
    function getAllEmail($hash, $datePath){
        $result =[];
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $serviceEmail = $em->getRepository(ServiceEmail::class)->findBy(['hash' => $hash]);
        $path = "./public/service/$hash";
        $datePath = explode("-",$datePath);
        foreach ($serviceEmail as $email){
            $userObject = $em->getRepository(UserEmail::class)->findOneBy(['email' => $email->getEmail()]);
            $userId = ($userObject) ? $userObject->getUserId() : 0;
            $dirPath = $email->getEmail()."_".$userId."/".$datePath[0]."/".$datePath[1]."/".$datePath[2];
            $dir = "$path/".$dirPath;
            $images=[];
            if(file_exists($dir)){
                $images = scandir($dir);
            }
            $result[]=[
                'email' => $email->getEmail(),
                'userId' => $userId,
                'images' => $images,
                'url' => $this->url()->fromRoute("service-room/image",['hash'=>$hash,'emailId'=>$email->getEmail()."_".$userId,'dateImage'=>date("Y-m-d")]),
                'active' => count($images)
                ];
        }
        return $result;
    }

    function getPathFile($hash,$emailId){
        /*
         ./data/service/hash/@_id/2017/01/13/
        */
        $path = "./public/service";
        if(!file_exists($path)){
            mkdir($path);
        }
        if(!file_exists($path."/".$hash)){
            mkdir($path."/".$hash);
        }
        $pathEmail = $path."/".$hash."/".$emailId;
        if(!file_exists($pathEmail)){
            mkdir($pathEmail);
        }
        if(!file_exists($pathEmail."/".date('Y'))){
            mkdir($pathEmail."/".date('Y'));
        }
        if(!file_exists($pathEmail."/".date('Y')."/".date('m'))){
            mkdir($pathEmail."/".date('Y')."/".date('m'));
        }
        $pathDate = $pathEmail."/".date('Y')."/".date('m')."/".date('d');
        if(!file_exists($pathDate)){
            mkdir($pathDate);
        }

        return $pathDate;

    }

    function getToken($hash,$email){
        $token = base64_encode(openssl_random_pseudo_bytes(3) .$hash .date("Y-m-d H:i:s"). $email);
        return $token;
    }

    function connectRedmine($url, $apiKey = 0, $login = 0, $pass = 0)
    {
        if (empty($url)) return false;
        $client = false;

        if ($apiKey) {
            $client = new Redmine\Client($url, $apiKey);
        }

        if ($login && $pass) {
            $client = new Redmine\Client($url, $login, $pass);
        }
        if ($client) {
            $result = $client->user->all();
            if (is_array($result)) {
                foreach ($result['users'] as $user) {
                    if ($apiKey) {
                        return $apiKey;
                    }
                    if ($user['login'] == $login) {
                        return $client->user->show($user['id'])['user']['api_key'];
                    }
                }
            }
        }

        return false;

    }
}
//        $form = [
//            'form'=>[
//                'metod' => "POST",
//                'action' => $url
//            ],
//            'input'=>[
//                0 =>[
//                    'name' => "name",
//                    'type' => "text",
//                    'value' => "test",
//                    'label' => [
//                        'value' => "label test"
//                    ],
//                    'valid' => null,
//                    'message' => ""
//                ]
//            ],
//            'select' =>[
//                0=>[
//                    'name' => "select_name",
//                    'value' => "test",
//                    'option' =>[
//                        1=>"op1",
//                        2=>"op2"
//                    ],
//                    'label' => [
//                        'value' => "label test"
//                    ],
//                    'valid' => null,
//                    'message' => ""
//                ]
//            ]
//        ];