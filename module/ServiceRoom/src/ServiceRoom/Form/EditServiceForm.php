<?php
namespace ServiceRoom\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class EditServiceForm extends Form
{
    public function __construct()
    {
        // Определяем имя формы
        parent::__construct('service-edit-form');

        // Задаем метод POST для этой формы
        $this->setAttribute('method', 'post');
        // Добавляем элементы формы
        $this->addinputFilter();
        $this->addElements();
    }

    private function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class' => 'form-control'
            ],
            'options' => [
                'label' => 'Service name ',
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'edit',
            'attributes' => [
                'value' => 'Сохранить ',
                'class' => 'btn btn-success btn-sm pull-right'
            ],
        ]);
    }
    private function addinputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        $inputFilter->add([
            'name' => 'name',
            'required' => true,


            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [   'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 255
                    ],
                ],
            ],

        ]);


    }
}