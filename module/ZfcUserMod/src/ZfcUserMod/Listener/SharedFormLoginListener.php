<?php
namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use ZfcUser\Form\LoginFilter;
use ZfcUser\Form\Login as LoginForm;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
class SharedFormLoginListener implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    protected 
        $listeners = array(),
        $serviceLocator;
    
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
             'ZfcUser\Service\User', 
             'login', 
             array($this, 'onLog'), 
             100
         );
        
        /*
         * создание формы Login
         * */
        $this->listeners[] = $events->attach(
            'ZfcUser\Form\Login',
            'init',
            array($this, 'onInit')
        );
        
        /*
         * создание LoginFilter
         * */
        $this->listeners[] = $events->attach(
            'ZfcUser\Form\LoginFilter',
            'init',
            array($this, 'onInitLoginFilter')
        );
    }
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    function onLog(){
//         echo 'onLog, ';
    }
    
    function onInit(EventInterface $e){
//         echo 'onInit';
        // Inject SM into validator manager
//         echo '\n';
//         $FormLogin = $e->getTarget();
//         echo get_class($FormLogin);
//         $FormLogin->setAttribute('class', 'login-form');
        
//         $validatorChain = $factory->getDefaultValidatorChain();
//         $validatorChain->setPluginManager($pm);
        
    }
    
    /*
     * Проверка данных, по какой причине мы не можем залогиниться
     * Вместо FlashMessager
     * */
    function onInitLoginFilter(EventInterface $e){
//         echo 'onInitLoginFilter: ';
        
        $LoginFilter = $e->getTarget();
        
        if ($LoginFilter instanceof LoginFilter) {
            $InputEmail = $LoginFilter->get('identity');
            $InputEmail->getValidatorChain()->attach($this->getServiceLocator()->get('ZfcUserMod\Validator\ZfcUserLogin'));
        }
        
        
        
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
}

?>