<?php

namespace ZfcUserMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class SharedFormRegistrationListener implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    protected $listeners = [];
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
            'ZfcUser\Service\User',
            'register',
            [$this, 'register'],
            1000
        );
        
        $this->listeners[] = $events->attach(
            'ZfcUser\Service\User',
            'register.post',
            [$this, 'registerPost'],
            100
        );
    }
    
    public function detachShared(SharedEventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    public function register(EventInterface $e)
    {
    }
    
    public function registerPost(EventInterface $e)
    {
        
        $CPM = $this->getServiceLocator()->get('ControllerPluginManager');
        $flashMessenger = $CPM->get('flashMessenger');
        $UserEntity = $e->getParam('user');
        $flashMessenger->addSuccessMessage($UserEntity->getEmail());
    }
    
}