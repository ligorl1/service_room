<?php

namespace ZfcUserMod;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\SharedEventManager;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Debug\Debug;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUserMod\Form\Register;
use ZfcUserMod\Form\RegisterFilter;
use ZfcUserMod\Validator\ZfcUserLogin as ZfcUserLoginValidator;
use Zend\ModuleManager\ModuleManager;
use Zend\ModuleManager\ModuleEvent;
use ZfcUserMod\Listener\SharedAdapterChainListener;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface
{
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ],
            ],
        ];
    }
    
    public function getConfig($env = null)
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function init(ModuleManager $moduleManager)
    {
        $events = $moduleManager->getEventManager();
        // Registering a listener at default priority, 1, which will trigger
        // after the ConfigListener merges config.
        $events->attach(ModuleEvent::EVENT_MERGE_CONFIG, [$this, 'onMergeConfig']);
        
    }
    
    /*
     * 'transport' => 'Zend\Mail\Transport\Smtp',
        'transport_options' => array(
            "name" => "Board",//Board
            'host' => 'mail.ukraine.com.ua',
            "port" => 25,
            'connection_class' => 'plain',
            'connection_config' => array(
                'username' => 'noreply@time-ismoney.com',
                'password' => '4KxNt2e1',
            ),
        ),
     * */
    public function onMergeConfig(ModuleEvent $e)
    {
        $configListener = $e->getConfigListener();
        $config = $configListener->getMergedConfig(false);
        
        // Modify the configuration; here, we'll remove a specific key:
        if (isset($config['mt_mail']['transport'])) {
            $config['goaliomailservice']['type'] = $config['mt_mail']['transport'];
        }
        
        // Modify the configuration; here, we'll remove a specific key:
        if (isset($config['mt_mail']['transport_options'])) {
            $config['goaliomailservice']['options_class'] = 'Zend\Mail\Transport\SmtpOptions';
            $config['goaliomailservice']['options'] = $config['mt_mail']['transport_options'];
            
            //goalio-email
            $config['goalioforgotpassword']['email_from_address']['email']
                = $config['mt_mail']['transport_options']['connection_config']['username'];
            $config['goalioforgotpassword']['email_from_address']['name']
                = $config['mt_mail']['transport_options']['name'];
            $config['goalioforgotpassword']['email_format'] = 'html';
        }
        
        // Pass the changed configuration back to the listener:
        $configListener->setMergedConfig($config);
    }
    
    public function getServiceConfig()
    {
        return [
            'invokables'   => [
                'ZfcUserMod\Listener\SharedFormChangeEmail' => 'ZfcUserMod\Listener\SharedFormChangeEmail',
                'ZfcUserMod\Listener\SharedFormLoginListener'
                                                            => 'ZfcUserMod\Listener\SharedFormLoginListener',
                /*
                 * recovery email
                 * */
                'ZfcUserMod\Listener\SharedFormRecoveryListener'
                                                            => 'ZfcUserMod\Listener\SharedFormRecoveryListener',
                
                'ZfcUserMod\Listener\SharedFormRegistrationListener'
                => 'ZfcUserMod\Listener\SharedFormRegistrationListener',
                
                'ZfcUserMod\Listener\SharedMapperUserListener'
                => 'ZfcUserMod\Listener\SharedMapperUserListener',//ZfcUserMod\Listener\SharedMapperUserListener
            ],
            'factories'    => [
                'ZfcUserMod\Validator\ZfcUserLogin' => function (ServiceLocatorInterface $ServiceLocator) {
                    $ZfcUserLoginValidator = new ZfcUserLoginValidator();
                    $ZfcUserLoginValidator->setServiceLocator($ServiceLocator);
                    
                    return $ZfcUserLoginValidator;
                },
                'ZfcUserMod\Listener\SharedAdapterChainListener' => function (ServiceLocatorInterface $ServiceLocator) {
                    $SharedAdapterChainListener = new SharedAdapterChainListener();
                    $SharedAdapterChainListener->setServiceLocator($ServiceLocator);
                    
                    return $SharedAdapterChainListener;
                },
                
                /* Перекрываем ZFC форму регистрации своей */
                'ZfcUserMod\InputFilter\RegisterFilter' => function (ServiceLocatorInterface $ServiceLocator) {
                    $registerFilter = new RegisterFilter($ServiceLocator);
                    
                    return $registerFilter;
                },
                'zfcuser_register_form' => function (ServiceLocatorInterface $ServiceLocator) {
                    $options = $ServiceLocator->get('zfcuser_module_options');
                    $form = new Register(null, $options);
    
                    $form->setInputFilter($ServiceLocator->get('ZfcUserMod\InputFilter\RegisterFilter'));
                    
                    return $form;
                },
            ],
            'initializers' => [
                function ($service, ServiceLocatorInterface $SL) {
                    if ($service instanceof ServiceLocatorAwareInterface) {
                        $service->setServiceLocator($SL);
                    }
                },
            ],
        ];
    }
    
    public function onBootstrap(MvcEvent $e)
    {
        $ServiceLocator = $e->getApplication()->getServiceManager();
        
        //      Debug::dump($ServiceLocator->get('Config')['view_manager']['template_path_stack']);
        //      Debug::dump($ServiceLocator->get('Config')['goaliomailservice']);
        
        $SEM = $e->getApplication()->getEventManager()->getSharedManager();
        
        if ($SEM instanceof SharedEventManager) {
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedFormLoginListener'));
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedFormRecoveryListener'));
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedFormRegistrationListener'));
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedMapperUserListener'));
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedAdapterChainListener'));
            $SEM->attachAggregate($ServiceLocator->get('ZfcUserMod\Listener\SharedFormChangeEmail'));
        }
        
        /*
         * fix для запрета на url
         * */
        $e->getApplication()->getEventManager()->getSharedManager()->attach(
            'Zend\Mvc\Controller\AbstractActionController',
            MvcEvent::EVENT_DISPATCH,
            function ($e) {
                $controller = $e->getTarget();
                $lang = $controller->params()->fromRoute('lang');
                
                $routeMatch = $e->getRouteMatch();
                
                if ('zfcuser' === $routeMatch->getMatchedRouteName()
                    || 'scn-social-auth-user' === $routeMatch->getMatchedRouteName()
                ) {
//                    $app = $e->getApplication();
//                    $sm = $app->getServiceManager();
//                    $auth = $sm->get("zfcuser_auth_service");
                    
//                    $controller->plugin('redirect')->toRoute(
//                        'zfcuser/user_profile',
//                        [
//                            'user_id' => $auth->getIdentity()->getId(),
//                            'lang'    => $lang,
//                        ]
//                    );
                }
            }
        );
        
        
        /*
         * MtMailFix
         */
        //           $e->getApplication()
        //             ->getEventManager()
        //             ->attach(MvcEvent::EVENT_ROUTE, function ($e) {
        //                 $translate =
        //                     $e->getApplication()->getServiceManager()->get('ViewHelperManager')->get('translate');
        //                 $Config =
        //                     $e->getApplication()->getConfig();
        //                 $Config['ht_user_registration']['verification_email_subject'] = $translate($Config['ht_user_registration']['verification_email_subject']);
        //                 $Config['ht_user_registration']['password_request_email_subject'] = $translate($Config['ht_user_registration']['password_request_email_subject']);
        //             },-1001);
    }
}
