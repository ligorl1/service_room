<?php

return array(
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../src/ZfcUserMod/language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'zfcuser' => __DIR__ . '/../view',
            'goalioforgotpassword' => __DIR__ . '/../view',
            'password_request_email_template' => __DIR__ . '/../view',
            //
            'verification_email_template' => __DIR__ . '/../view/ht-user-registration/mail/verify-email.phtml',
        ),
        'template_map' => [
            'zfc-user/user/login' => __DIR__ . '/../view/zfc-user/user/login.phtml',
        ]
    ),

    /*
     * 
     * */
    
    'controllers' => array(
        'invokables' => array(
            'ZfcUserMod\Controller\Recovery'
                => 'ZfcUserMod\Controller\RecoveryController'
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'zfcuser' => array(
                'type' => 'Segment',
                'priority' => 1000,
                'options' => array(
                    'constraints' => array(
                        'lang' => '[a-z]{2}?',
                    ),
                    'route' => '/:lang/user',
                    'defaults' => array(
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                        'lang'   => 'ru'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    
                    'recovery' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/recovery',//поторное получение письма подтверждения
                            'defaults' => array(
                                'controller' => 'ZfcUserMod\Controller\Recovery',
                                'action'     => 'index',
                            ),
                        ),
                    ),
                    
                    'login' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'login',
                            ),
                        ),
                    ),
                    
                    'login-partner' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login-partner',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'login',
                            ),
                        ),
                    ),
                    
                    'authenticate' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/authenticate',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'authenticate',
                            ),
                        ),
                    ),
                    'logout' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'logout',
                            ),
                        ),
                    ),
                    'register' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/register',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'register',
                            ),
                        ),
                    ),
//                    'register-partner' => array(
//                        'type' => 'Literal',
//                        'options' => array(
//                            'route' => '/register-partner',
//                            'defaults' => array(
//                                'controller' => 'zfcuser',
//                                'action'     => 'register',
//                            ),
//                        ),
//                    ),
                    'changepassword' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/change-password',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action'     => 'changepassword',
                            ),
                        ),
                    ),
                    'changeemail' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/change-email',
                            'defaults' => array(
                                'controller' => 'zfcuser',
                                'action' => 'changeemail',
                            ),
                        ),
                    ),
                ),
            ),
            
        ),
    ),
);
