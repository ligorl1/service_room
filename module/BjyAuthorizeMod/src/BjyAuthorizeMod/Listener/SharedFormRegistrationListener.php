<?php
namespace BjyAuthorizeMod\Listener;
use BjyAuthorizeMod\Entity\UserRoleLinker;
use BjyAuthorizeMod\Mapper\UserRole;
use Zend\EventManager\SharedListenerAggregateInterface;use Zend\Debug\Debug;
use Zend\EventManager\SharedEventManagerInterface;
use Zend\EventManager\EventInterface;
use ZfcUser\Mapper\User as UserMapper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
class SharedFormRegistrationListener implements SharedListenerAggregateInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    protected $listeners = array();
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
            'ZfcUser\Service\User',
            'register.post',//после добавления
            array($this, 'postRegister')
        );
        
        $this->listeners[] = $events->attach(
            'ZfcUser\Service\User',
            'register',
            array($this, 'onLog')
        );
    }

    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    function onLog(){
        echo 'onLog';
    }
    
    function postRegister(EventInterface $e) {
        /* Добавить РОЛЬ по умолчанию */
        $serviceLocator = $this->getServiceLocator();
        $userEntity =  $e->getParam('user');
        $registerForm = $e->getParam('form');
        
        /* Получаем account_type из формы */
        $accountType = $registerForm->get('account_type')->getValue();
        
        /* Получить ID зарегистрированного пользователя */
        $zfcuser_user_mapper = $serviceLocator->get('zfcuser_user_mapper');

        if ($zfcuser_user_mapper instanceof UserMapper) {
            $entityUser = $zfcuser_user_mapper->findByEmail($userEntity->getEmail());
        } else{
            return false;
        }

        /* Получаем роль пользователя */
        /** @var UserRole $userRoleMapper */
        $userRoleMapper = $serviceLocator->get('BjyAuthorizeMod\Mapper\UserRole');
        $userRoleEntity = $userRoleMapper->getEntityListByFilter(['role_id' => $accountType])->current();
    
        /* если роль не найдена - устанавливаем роль по умолчанию */
        if(empty($userRoleEntity)) {
            $userRoleEntity = $userRoleMapper->getEntityListByFilter([
                'is_default' => 1
            ])->current();
        }
        
        /* Установить роль для зарегистрированных пользователей */
        $userRoleLinkerMapper = $serviceLocator->get('BjyAuthorizeMod\Mapper\UserRoleLinker');
        /** @var UserRoleLinker $userRoleLinkerEntity */
        $userRoleLinkerEntity = $userRoleLinkerMapper->getEntityPrototype();
        //Установить значение ID того email который есть в системе и USER_ROLE по умолчанию
        $userRoleLinkerEntity->setUserId($entityUser->getId());
        $userRoleLinkerEntity->setRoleId($userRoleEntity->getRoleId());

        $resultInsert = $userRoleLinkerMapper->insert($userRoleLinkerEntity);
        return $resultInsert;
    }
   
}