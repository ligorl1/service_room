<?php
namespace BjyAuthorizeMod\Listener;

use Zend\EventManager\SharedEventAggregateAwareInterface;
use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
class SharedRegistrationListener implements SharedListenerAggregateInterface
{
    protected $listeners = array();
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
             'ZfcUser\Service\User', 
             'register', 
             array($this, 'onLog'), 
             100
         );
    }
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    function onLog(){
        echo 'onLog';
    }
}

?>