<?php
namespace BjyAuthorizeMod\Listener;

use Zend\EventManager\SharedListenerAggregateInterface;
use Zend\EventManager\SharedEventManagerInterface;
class SharedMapperUserListener implements SharedListenerAggregateInterface
{
    protected $listeners = array();
    
    public function attachShared(SharedEventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(
             'ZfcUser\Mapper\User', 
             'find', 
             array($this, 'find')
         );
    }
    
    public function detachShared(SharedEventManagerInterface  $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    function find(){
//         echo 'find :)';
    }
}

