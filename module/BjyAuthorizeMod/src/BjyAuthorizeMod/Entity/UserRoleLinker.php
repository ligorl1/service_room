<?php
namespace BjyAuthorizeMod\Entity;

class UserRoleLinker
{
    protected $userId, $roleId;
    
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    
    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->roleId;
    }
    
    /**
     * @param mixed $roleId
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;
    }
    
    
}