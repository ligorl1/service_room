<?php

namespace ServiceInventory;

use DoctrineModule\Service\DriverFactory;
use DoctrineORMModule\Service\DBALConnectionFactory;
use DoctrineORMModule\Service\EntityManagerFactory;
use ServiceRoom\Service\ImageManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;
return array(

    'doctrine' => array(

        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_inventory' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            ),
        ),



    ),


    'controllers' => array(
        'invokables' => array(
            'ServiceInventory\Controller\Admin' => Controller\AdminController::class,
        ),
        'factories' => array(

        ),
    ),
    'router' => array(
        'routes' => array(
            'json' => array(
                'may_terminate' => true,
                'child_routes' => array(

                    'inventory' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '/inventory',
                            'defaults' => array(
                                '__NAMESPACE__' => 'ServiceInventory\Controller',
                                'controller'    => 'Admin',
                                'action'        => 'admin',
                            ),
                        ),
                    ),
                )
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
        ),
        'aliases' => [
            'doctrine.configuration.orm_inventory' => 'doctrine.configuration.orm_default',
        ],
        'factories' => [
            'doctrine.connection.orm_inventory'    => new DBALConnectionFactory('orm_inventory'),
            'doctrine.entitymanager.orm_inventory' => new EntityManagerFactory('orm_inventory'),
        ],
        'invokables' =>[
        ],
        'abstract_factories' => array(

        )
    ),
    'view_helpers' => array(
        'invokables' => array(
        ),

    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(

            //'service-inventory/admin/admin' => __DIR__ . '/../view/admin/admin.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies'=>array('ViewJsonStrategy'),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
