<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\PriceSymbol;
use Doctrine\ORM\EntityRepository;
use Entities;


class PriceSymbolRepository extends EntityRepository
{
    public function findSymbol(){
        $result = [];
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('ps')
            ->from(PriceSymbol::class, 'ps');

        $symbols = $queryBuilder->getQuery()->getArrayResult();
        if($symbols) {
            foreach ($symbols as $symbol) {
                $result[$symbol['symbol']] = $symbol['symbol'];
            }
        }

        return $result;
    }

    public function isSymbol($symbol)
    {

        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('ps')
            ->from(PriceSymbol::class, 'ps')
            ->where('ps.symbol= :symbol')
            ->setParameter('symbol', $symbol);


        return empty($queryBuilder->getQuery()->getArrayResult()) ? false : true;
    }

    }