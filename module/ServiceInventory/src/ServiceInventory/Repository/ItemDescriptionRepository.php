<?php
namespace ServiceInventory\Repository;

use ServiceInventory\Entity\ItemDescription;
use Doctrine\ORM\EntityRepository;
use Entities;


class ItemDescriptionRepository extends EntityRepository
{

    public function getStatusItem($itemID){

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('id')
            ->from(ItemDescription::class, 'id')
            ->where("id.items = :id")
            ->orderBy('id.date', 'DESC')
            ->setParameter('id',$itemID)
            ->setMaxResults(1);

        return $qb->getQuery()->getSingleResult();

    }


}