<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="computer_foto")
 */
class ComputerFoto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;
    /**
     * @ORM\Column(name="url")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Computer", inversedBy="computerFoto", cascade={"persist"})
     * @ORM\JoinColumn(name="computer_id", referencedColumnName="id")
     */
    private $computer;


    /*
     * @return \Inventory\Entity\Computer
     */
    public function getComputer()
    {
        return $this->computer;
    }

    /**
     * @param \ServiceInventory\Entity\Computer $computer
     */
    public function setComputer($computer)
    {
        $this->computer = $computer;
        $computer->addComputerFoto($this);
    }



    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setId($id)
    {
        $this->id = $id;
    }



}