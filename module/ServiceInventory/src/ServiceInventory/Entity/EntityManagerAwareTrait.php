<?php
/**
 * Created by PhpStorm.
 * User: user1
 * Date: 29.03.2017
 * Time: 10:19
 */

namespace ServiceInventory\Entity;


use DoctrineORMModule\Options\EntityManagerInterface;

trait EntityManagerAwareTrait
{
    /** @var EntityManagerInterface $entityManager */
    protected $entityManager;

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}