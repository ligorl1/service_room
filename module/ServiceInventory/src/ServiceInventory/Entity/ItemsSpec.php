<?php
namespace ServiceInventory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ItemSpecRepository")
 * @ORM\Table(name="item_spec")
 */
class ItemsSpec
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

//    /**
//     * @ORM\Column(name="type_item_type")
//     */
//    protected $typeItemType;

    /**
     * @ORM\Column(name="name",unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\Item", mappedBy="itemsSpec", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="item_spec_id")
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\TypeItem", inversedBy="itemsSpec")
     * @ORM\JoinColumn(name="type_item_id", referencedColumnName="id")
     */
    private $typeItem;
    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\SpecFoto", mappedBy="itemSpec")
     * @ORM\JoinColumn(name="id", referencedColumnName="item_spec_id")
     */
    protected $specFoto;


    /*
     * Возвращает связанный пост.
     * @return \ServiceInventory\Entity\TypeItem
     */
    public function getTypeItem()
    {
        return $this->typeItem;
    }

    /**
     * Задает связанный пост.
     * @param \ServiceInventory\Entity\TypeItem $typeItem
     */
    public function setTypeItem($typeItem)
    {
        $this->typeItem = $typeItem;
        $typeItem->addItemsSpec($this);
    }


    public function __construct()
    {
       $this->items = new ArrayCollection();
        $this->specFoto = new ArrayCollection();
    }

    public function getSpecFoto()
    {
        return $this->specFoto;
    }


    public function addSpecFoto($specFoto)
    {
        $this->specFoto[] = $specFoto;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param $items
     */
    public function addItems($items)
    {
        $this->items[] = $items;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


//    public function getTypeItemType()
//    {
//        return $this->typeItemType;
//    }
//
//    public function setTypeItemType($typeItemType)
//    {
//        $this->typeItemType = $typeItemType;
//    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

}