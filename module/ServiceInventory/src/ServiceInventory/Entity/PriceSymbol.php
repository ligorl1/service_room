<?php
namespace ServiceInventory\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\PriceSymbolRepository")
 * @ORM\Table(name="price_symbol")
 */
class PriceSymbol
{
    /**
     * @ORM\Id
     * @ORM\Column(name="symbol")
     */
    private $symbol;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ItemsPrice", mappedBy="priceSymbol")
     * @ORM\JoinColumn(name="symbol", referencedColumnName="price_symbol")
     */
    private $itemsPrice;

    public function __construct()
    {
        $this->itemsPrice = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getItemsPrice()
    {
        return $this->itemsPrice;
    }

    /**
     * @param $itemsPrice
     */
    public function addItemsPrice($itemsPrice)
    {
        $this->itemsPrice[] = $itemsPrice;
    }


    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

}