<?php
namespace ServiceInventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="ServiceInventory\Repository\ItemsRepository")
 * @ORM\Table(name="item")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id")
     */
    private $id;

    /**
     * @ORM\Column(name="sn")
     */
    private $sn;


    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $orders;

    /**
     * @ORM\ManyToOne(targetEntity="\ServiceInventory\Entity\ItemsSpec", inversedBy="items")
     * @ORM\JoinColumn(name="item_spec_id", referencedColumnName="id")
     */
    private $itemsSpec;

    /**
     * @ORM\Column(name="start_date" , nullable=true,type="datetime")
     */
    private $startDate;
    /**
     * @ORM\Column(name="finish_date", nullable=true,type="datetime")
     */
    private $finishDate;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ComputerItem", mappedBy="items")
     * @ORM\JoinColumn(name="id", referencedColumnName="item_id")
     */
    private $computerItem;
    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ItemsPrice", mappedBy="items")
     * @ORM\JoinColumn(name="id", referencedColumnName="item_id")
     */
    private $itemsPrice;

    /**
     * @ORM\ManyToMany(targetEntity="\ServiceInventory\Entity\User", inversedBy="items")
     * @ORM\JoinTable(name="user_item",
     *      joinColumns={@ORM\JoinColumn(name="item_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="\ServiceInventory\Entity\ItemDescription", mappedBy="items")
     * @ORM\JoinColumn(name="id", referencedColumnName="item_id")
     */
    private $itemDescription;

    public function __construct()
    {
        $this->computerItem = new ArrayCollection();
        $this->user = new ArrayCollection();

        $this->itemsPrice = new ArrayCollection();
        $this->itemDescription = new ArrayCollection();
    }


    public function getUser()
    {
        return $this->user;
    }

    public function addUser($user)
    {
        $this->user[] = $user;
    }

    public function removeUserAssociation($user)
    {
        $this->user->removeElement($user);
    }

    /**
     * @return array
     */
    public function getComputerItem()
    {
        return $this->computerItem;
    }

    /**
     * @param $computerItem
     */
    public function addComputerItem($computerItem)
    {
        $this->computerItem[] = $computerItem;
    }

    /**
     * @return array
     */
    public function getItemsPrice()
    {
        return $this->itemsPrice;
    }


    /**
     * @param $itemsPrice
     */
    public function addItemsPrice($itemsPrice)
    {
        $this->itemsPrice[] = $itemsPrice;
    }

    /**
     * @return mixed
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     * @param mixed $itemDescription
     */
    public function addItemDescription($itemDescription)
    {
        $this->itemDescription[] = $itemDescription;
    }

    /*
     * @return \ServiceInventory\Entity\ItemsSpec
     */
    public function getItemsSpec()
    {
        return $this->itemsSpec;
    }

    /**
     * @param \ServiceInventory\Entity\ItemsSpec $itemSpec
     */
    public function setItemsSpec($itemsSpec)
    {
        $this->itemsSpec = $itemsSpec;
        $itemsSpec->addItems($this);
    }

    /*
     * @return \ServiceInventory\Entity\Orders
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param \ServiceInventory\Entity\Order $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
        $orders->addItems($this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


    public function getSn()
    {
        return $this->sn;
    }

    public function setSn($sn)
    {
        $this->sn = $sn;
    }


    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getFinishDate()
    {
        return $this->finishDate;
    }

    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;
    }

}