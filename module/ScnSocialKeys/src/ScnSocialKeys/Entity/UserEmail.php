<?php

namespace ScnSocialKeys\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_email")
 */
class UserEmail
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="id")
     *
     */
    protected $id;
    /**
     * @var int
     * @ORM\Column(type="integer", name="user_id")
     *
     */
    protected $userId;
    /**
     * @var string
     * @ORM\Column(name="email",type="string", length=50, unique=true)
     */
    protected $email;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $userId
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @ORM\OneToOne(targetEntity="\ScnSocialKeys\Entity\Email", inversedBy="emailUser",cascade={"persist"})
     * @ORM\JoinColumn(name="email", referencedColumnName="email")
     */
    private $emailSingle;

    /**
     * @return mixed
     */
    public function getEmailSingle()
    {
        return $this->emailSingle;
    }

    /**
     * @param mixed $emailSingle
     */
    public function setEmailSingle($emailSingle)
    {
        $this->emailSingle = $emailSingle;
    }


}
