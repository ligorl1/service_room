<?php
/**
 * ScnSocialAuth Module
 *
 * @category   ScnSocialAuth
 * @package    ScnSocialAuth_Service
 */

namespace ScnSocialKeys\Service;

use ScnSocialKeys\Controller\SocController;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @category   ScnSocialAuth
 * @package    ScnSocialAuth_Service
 */
class UserSocControllerFactory extends \ScnSocialAuth\Service\UserControllerFactory
{
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $mapper = $controllerManager->getServiceLocator()->get('ScnSocialAuth-UserProviderMapper');
        $moduleOptions = $controllerManager->getServiceLocator()->get('ScnSocialAuth-ModuleOptions');
        $redirectCallback = $controllerManager->getServiceLocator()->get('zfcuser_redirect_callback');

        $controller = new SocController($redirectCallback);
        $controller->setMapper($mapper);
        $controller->setOptions($moduleOptions);

        try {
          $hybridAuth = $controllerManager->getServiceLocator()->get('HybridAuth');
          $controller->setHybridAuth($hybridAuth);
        } catch (\Zend\ServiceManager\Exception\ServiceNotCreatedException $e) {

        }

        return $controller;
    }
}
