<?php
namespace ScnSocialKeys\View\Helper;


use ScnSocialKeys\Entity\UserProvider;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class SocialSignInButton extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use \Zend\ServiceManager\ServiceLocatorAwareTrait;

    public function __invoke($provider, $redirect = false)
    {
        $name = 'scn-social-auth-user/authenticate/provider';
        $params = array('provider' => $provider);
        $options = array();

        if ($redirect) {
            $options = array(
                'query' => array(
                    'redirect' => $redirect,
                ),
            );
        }
        $session = new Container("Zend_Auth");
        $userId = $session->storage;
        $em = $this->getServiceLocator()->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->getRepository(UserProvider::class)->findOneBy(['userId' => $userId, 'provider'=>$provider]);
        if(empty($user)) {
            $url = $this->view->url($name, $params, $options);
            echo '<a class="btn btn-success" href="' . $url . '">' . ucfirst("Подключить") . '</a>';
        }else{
            $url = $this->view->url('keys/keys_delete', ['provider'=>$provider]);
            echo '<a class="btn btn-primary" href="' . $url . '">' . ucfirst("Отключить") . '</a>';
        }
    }
}
