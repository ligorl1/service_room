<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Voodoo773Localization for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Voodoo773Localization\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;
use Zend\Json\Json;
use Zend\Debug\Debug;
use Zend\Http\PhpEnvironment\Response;
use ZfcUserMod\Form\Register;

class IndexController extends AbstractActionController
{
    protected $translator;
    
    public function resourcesAction()
    {
        /*Получаем все морские порты*/
        $portMapper = $this->getServiceLocator()->get('Acex\Mapper\Port');
        $portEntityList = $portMapper->getEntityList()->buffer();
        
        /*Получаем аэропорты*/
        $uploadMapper = $this->getServiceLocator()->get('Voodoo773Localization\Mapper\UploadMapper');
        $airportEntityList = $uploadMapper->getEntityList()->buffer();
        
        $seaPortMarkers = [];
        /*Вывод меток всех морских портов*/
        foreach ($portEntityList as $portEntity) {
            $seaPortMarkers[] = [
                'label'     => $portEntity->getLabel(),
                'latitude'  => $portEntity->getLatitude(),
                'longitude' => $portEntity->getLongitude(),
            ];
        }
        /*Вывод меток всех аэропортов*/
        $airPortMarkers = [];
        foreach ($airportEntityList as $airportEntity) {
            $airPortMarkers[] = [
                'iata'      => $airportEntity->getIata(),
                'name'      => $airportEntity->getNameEng(),
                'city'      => $airportEntity->getCityEng(),
                'iso'       => $airportEntity->getIsoCode(),
                'latitude'  => $airportEntity->getLatitude(),
                'longitude' => $airportEntity->getLongitude(),
            ];
        }
        $seaPortMarkersJson = Json::encode($seaPortMarkers);
        $airPortMarkersJson = Json::encode($airPortMarkers);
        
        return new ViewModel([
            'seaPortMarkersJson' => $seaPortMarkersJson,
            'airPortMarkersJson' => $airPortMarkersJson,
        ]);
    }
    
    public function signInAction()
    {
    }
    
    public function becomePartnerAction()
    {
        $this->layout('layout/partner');
        
        /** @var Register $form */
        $form = $this->getServiceLocator()->get('zfcuser_register_form');
        
        //set action to zfcuser register action
        $form->setAttribute('action', $this->url()->fromRoute('zfcuser/register', ['lang' => $this->lang()]));
        $form->setAttribute('method', 'POST');
        $form->setAttribute('autocomplete', 'off');
        $form->get('account_type')->setAttribute('value', 'user-partner');
        
        return new ViewModel([
            'form' => $form,
        ]);
    }
    
    public function startUsingAction()
    {
    }
    
    public function listsAction()
    {
    }
    
    public function contactAction()
    {
        $form = $this->getServiceLocator()->get('Voodoo773Localization\Factory\Contact');
        /** @var \Voodoo773Localization\Mapper\Contact $contactMapper */
        $contactMapper = $this->getServiceLocator()->get('Voodoo773Localization\Mapper\ContactMapper');
        $contactEntity = $contactMapper->getEntityPrototype();
        
        $url = $this->url()->fromRoute('localization/contact', ['lang' => $this->lang()]);
        $prg = $this->postRedirectGet($url, true);
        
        if ($prg instanceof Response) {
            return $prg;
        } elseif ($prg === false) {
            return [
                'form' => $form,
            ];
        }
        
        $form->bind($contactEntity);
        $form->setData($prg);
        
        if ($form->isValid()) {
            $translator = $this->getServiceLocator()->get('translator');
            try {
                $contactMapper->insert($contactEntity);
                $this->flashMessenger()->addSuccessMessage($translator->translate('Cообщение успешно отправлено'));
            } catch (\Exception $e) {
                $this->flashMessenger()->addErrorMessage($translator->translate('Ошибка! Cообщение не отправлено')
                    . $e->getCode() . " - " . $e->getMessage());
            }
            
            /* trigger an event */
            $contactMapper->getEventManager()->trigger(__FUNCTION__ . '.post', null, ['contact' => $contactEntity]);
        } else {
            $this->flashMessenger()->addErrorMessage($form->getMessages());
        }
        
        return $this->redirect()->toUrl($url);
    }
    
    public function aboutAction()
    {
    }
    
    public function privacyAction()
    {
        //done
    }
    
    public function careersAction()
    {
        $form = $this->getServiceLocator()->get('Voodoo773Localization\Form\CareerForm');
        $careerMapper = $this->getServiceLocator()->get('Voodoo773Localization\Mapper\CareerMapper');
        $careerEntityPrototype = $careerMapper->getEntityPrototype();
        
        $translator = $this->getServiceLocator()->get('translator');
        
        $url = $this->url()->fromRoute('localization/careers', ['lang' => $this->lang()]);
        $prg = $this->postRedirectGet($url, true);
        
        if ($prg instanceof Response) {
            return $prg;
        } elseif ($prg === false) {
            return [
                'form' => $form,
            ];
        }
        
        $form->bind($careerEntityPrototype);
        $form->setData($prg);
        if ($form->isValid()) {
            try {
                $careerMapper->insert($careerEntityPrototype);
                $this->flashMessenger()->addSuccessMessage($translator->translate('Ваша заявка отправлена'));
            } catch (\Exception $e) {
                $this->flashMessenger()->addErrorMessage($e->getCode() . " - " . $e->getMessage());
            }
            
            /* trigger event */
            $careerMapper->getEventManager()->trigger(
                __FUNCTION__ . '.post',
                null,
                ['career' => $careerEntityPrototype]
            );
        } else {
            $this->flashMessenger()->addErrorMessage($form->getMessages());
        }
        
        return $this->redirect()->toUrl($url);
    }
    
    public function indexAction()
    {
        $impCodes = $this->getServiceLocator()->get('Admin/Mapper/CargoImpMapper')->getEntityList()->toArray();
        $impList = [];
        foreach ($impCodes as $impCode) {
            $impList[] = [
                'imp_code'  => $impCode['imp_code'],
                'imp_label' => $this->lang() == 'ru'
                    ?
                    '(' . $impCode['imp_code'] . ') ' . $impCode['label_ru']
                    :
                    '(' . $impCode['imp_code'] . ') ' . $impCode['label_en'],
            ];
        }
        
        $viewModel = new ViewModel([
            'countries'     => $this->getServiceLocator()->get('Acex\Mapper\Country')->getEntityList()->toArray(),
//             'airports'      => $this->getServiceLocator()->get('Acex\Mapper\Airport')
//                                          ->getEntityList()->toArray(),   //аэропорты WFR
            'airports'      => $this->getServiceLocator()->get('Admin\Model\FFService')->airportList(),
            //аэропорты из нашего списка
            'bulk_ports'    => $this->getServiceLocator()->get('Acex\Mapper\BulkPort')->getEntityList()->toArray(),
            'ports'         => $this->getServiceLocator()->get('Acex\Mapper\Port')->getEntityList()->toArray(),
            'impList'       => $impList,
            'hazardousList' => $this->getServiceLocator()->get('Admin\Mapper\HazardousMapper')->getEntityList()
                ->toArray(),
            'user_access'   => $this->RoleAccessPlugin()->init(),
            //
            //'city'          => $this->getServiceLocator()->get('Acex\Mapper\City')->getEntityList()->toArray(),
        ]);
        
        return $viewModel;
    }
    
    /**
     *
     * Загрузка файла формата xls
     */
    public function uploadAction()
    {
        $form = $this->getServiceLocator()->get('Voodoo773Localization\Factory\Upload');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $data = $post['image-file']['tmp_name'];
            $file = $this->uploadFile($data);
            $this->xlsToMysql($file);
        }
        
        return ['form' => $form];
    }
    
    protected function uploadFile($file)
    {
        $uploaddir = getcwd() . '../data/cache';
        $uploadfile = $uploaddir . '/' . (int)microtime(true) . '.xls';
        if (move_uploaded_file($file, $uploadfile)) {
            return $uploadfile;
        }
        
        return false;
    }
    
    public function getPhpExcel($file)
    {
        $file = 'C:\Users\Public\Server\htdocs\user1\localhost.acex\data\cache\airinfo.xls';
        $xXx = is_readable($file);
        $inputFileType = \PHPExcel_IOFactory::identify($file);
        
        return \PHPExcel_IOFactory::load($file);
    }
    
    public function xlsToMysql($file)
    {
        
        $objPHPExcel = $this->getPhpExcel($file);
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        /*
         * Рекурсивно обходим все строки таблицы Excel и формируем объект с полученными данными
         */
        $rowIterator = $sheet->getRowIterator();   //Debug::dump($rowIterator); die; 
        $arr = [];
        /*
         * Проходим по объекту $rowIterator и получаем доступ к каждой строке таблицы в отдельности
         */
        foreach ($rowIterator as $row) {
            
            if ($row->getRowIndex() != 1) { // В $row попадает объект одной строки таблицы Excel
                
                $cellIterator = $row->getCellIterator();
                foreach ($cellIterator as $cell) {
                    $cellPath = $cell->getColumn(); // В $cellPath получаем доступ к каждой ячейке в отдельности
                    if (isset($this->cells[$cellPath])) {
                        $arr[$row->getRowIndex()][$this->cells[$cellPath]] = $cell->getValue();
                    }
                }
            }
        }
        $UploadMapper = $this->getServiceLocator()->get('Voodoo773Localization\Mapper\UploadMapper');
        foreach ($arr as $arrEntity) {
            $UploadEntity = $UploadMapper->getEntityPrototype();
            
            $UploadEntity->setIata($arrEntity['iata']);
            $UploadEntity->setNameRus($arrEntity['name_rus']);
            $UploadEntity->setNameEng($arrEntity['name_eng']);
            $UploadEntity->setCityRus($arrEntity['city_rus']);
            $UploadEntity->setCityEng($arrEntity['city_eng']);
            $UploadEntity->setIsoCode($arrEntity['iso_code']);
            $UploadEntity->setLatitude($arrEntity['latitude']);
            $UploadEntity->setLongitude($arrEntity['longitude']);
            try {
                $UploadMapper->insert($UploadEntity);
            } catch (\Exception $e) {
                Debug::dump($e->getMessage());
                die;
            }
        }
        
        return true;
    }
    
    public $cells
        = [
            'A' => 'iata',
            'B' => 'name_rus',
            'C' => 'name_eng',
            'D' => 'city_rus',
            'E' => 'city_eng',
            'F' => 'iso_code',
            'G' => 'latitude',
            'H' => 'longitude',
        ];
}
