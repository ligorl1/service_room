<?php

namespace Voodoo773Localization\View\Helper;

use Zend\Session\Container as SessionContainer;
use Zend\View\Helper\AbstractHelper;

class Locale extends AbstractHelper
{
    public function __invoke()
    {
        $lang = $this->getView()->lang();
        
        $config = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('Config');
        
        return $config['lang'][$lang]['locale'] ?? 'ru_RU';
    }
}