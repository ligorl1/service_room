<?php
namespace Voodoo773Localization\View\Helper\Navigation;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class Resources extends DefaultNavigationFactory
{
    protected function getName()
    {
        return 'resources';
    }
    
    protected function getPages(ServiceLocatorInterface $serviceLocator)
    {
        if (null === $this->pages) {
            $configuration = $serviceLocator->get('Config');
            
            if (!isset($configuration['navigation'])) {
                throw new \InvalidArgumentException('Could not find navigation configuration key');
            }
            if (!isset($configuration['navigation'][$this->getName()])) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Failed to find a navigation container by the name "%s"',
                        $this->getName()
                    )
                );
            }
            
            $pages = $this->getPagesFromConfig($configuration['navigation'][$this->getName()]);
            $this->pages = $this->preparePages($serviceLocator, $pages);
            $lang = $serviceLocator->get('ViewHelperManager')->get('lang');
            
            foreach ($this->pages as $num => $page) {
                $this->pages[$num]['params']['lang'] = $lang();
            }
        }
        
        return $this->pages;
    }
}
