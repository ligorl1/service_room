<?php
namespace Voodoo773Localization\View\Helper\Navigation;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LangNavFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $navigation = new Lang();
        
        return $navigation->createService($serviceLocator);
    }
}
