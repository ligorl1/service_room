<?php

namespace Voodoo773Localization\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Upload extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->addElements();
    }

    public function addElements()
    {
        $file = new Element\File('image-file');
        $this->add($file);
    }
}
        
        
        