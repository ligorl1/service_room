<?php

namespace Voodoo773Localization\Form;

use Zend\Form\Form;
use Zend\Validator\EmailAddress;

class Contact extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('contact');
        
        $this->add(
            [
                'name'       => 'name',
                'attributes' => [
                    'type'        => 'text',
                    'class'       => 'form-control',
                    'placeholder' => 'Ваше имя',
                ],
                'filters'    => [
                    ['name' => 'StringTrim'],
                ],
            ]
        );
        
        $this->add(
            [
                'name'       => 'email',
                'attributes' => [
                    'type'        => 'email',
                    'class'       => 'form-control',
                    'placeholder' => 'Email',
                    'required'    => 'required',
                ],
                'filters'    => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'    => 'EmailAddress',
                        'options' => [
                            'messages' => [
                                EmailAddress::INVALID_FORMAT => 'Email address format is invalid',
                            ],
                        ],
                    ],
                ],
            ]
        );
        
        $this->add(
            [
                'name'       => 'subject',
                'attributes' => [
                    'type'        => 'text',
                    'class'       => 'form-control',
                    'placeholder' => 'Тема',
                ],
                'filters'    => [
                    ['name' => 'StringTrim'],
                ],
            ]
        );
        
        $this->add(
            [
                'name'       => 'comment',
                'attributes' => [
                    'type'        => 'textarea',
                    'class'       => 'form-control',
                    'placeholder' => 'Ваш комментарий',
                    'rows'        => '4',
                    'id'          => 'comment',
                    'required'    => 'required',
                ],
                'filters'    => [
                    ['name' => 'StringTrim'],
                ],
            ]
        );
        
        $this->add(
            [
                'type'       => 'Button',
                'name'       => 'button',
                'options'    => [
                    'label' => 'Отправить',
                ],
                'attributes' => [
                    'type' => 'submit',
                ],
            ]
        );
        
    }
}
        
        
        