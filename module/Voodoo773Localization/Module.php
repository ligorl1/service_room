<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Voodoo773Localization for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Voodoo773Localization;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\I18n\Translator;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Session\Container as ContainerSession;
use Zend\Validator\AbstractValidator;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class Module implements AutoloaderProviderInterface, ViewHelperProviderInterface
{
    
    public function getControllerPluginConfig()
    {
        return [
            'invokables' => [
                'lang' => 'Voodoo773Localization\Controller\Plugin\Lang',
            ],
        ];
    }
    
    public function getServiceConfig()
    {
        return [
            'invokables' => [
                //Form
                'Voodoo773Localization\Form\Career'        => 'Voodoo773Localization\Form\CareerForm',
                'Voodoo773Localization\Form\Contact'       => 'Voodoo773Localization\Form\Contact',
                'Voodoo773Localization\Form\Upload'        => 'Voodoo773Localization\Form\Upload',
                //Filter
                'Voodoo773Localization\Form\ContactFilter' => 'Voodoo773Localization\Form\ContactFilter',
                'Voodoo773Localization\Form\CareerFilter'  => 'Voodoo773Localization\Form\CareerFilter',
                // Mapper
                'Voodoo773Localization\Mapper\Contact'     => 'Voodoo773Localization\Mapper\Contact',
                'Voodoo773Localization\Mapper\Career'      => 'Voodoo773Localization\Mapper\Career',
                'Voodoo773Localization\Mapper\Upload'      => 'Voodoo773Localization\Mapper\Upload',
                // Entity
                'Voodoo773Localization\Entity\Contact'     => 'Voodoo773Localization\Entity\Contact',
                'Voodoo773Localization\Entity\Career'      => 'Voodoo773Localization\Entity\Career',
                'Voodoo773Localization\Entity\Upload'      => 'Voodoo773Localization\Entity\Upload',
            ],
            'factories'  => [
                
                'myNav'        => 'Voodoo773Localization\View\Helper\Navigation\MyNavigationFactory',
                'resourcesNav' => 'Voodoo773Localization\View\Helper\Navigation\ResourcesFactory',
                'acexNav'      => 'Voodoo773Localization\View\Helper\Navigation\AcexFactory',
                'langNav'      => 'Voodoo773Localization\View\Helper\Navigation\LangNavFactory',
                
                'Voodoo773Localization\Factory\Contact' => function ($sm) {
                    $form = $sm->get('Voodoo773Localization\Form\Contact');
                    $form->setObject($sm->get('Voodoo773Localization\Entity\Contact'));
                    $form->setInputFilter($sm->get('Voodoo773Localization\Form\ContactFilter'));
                    $form->setHydrator(new ClassMethods());
                    
                    return $form;
                },
                'Voodoo773Localization\Form\CareerForm' => function (ServiceLocatorInterface $sm) {
                    $form = $sm->get('Voodoo773Localization\Form\Career');//
                    $form->setObject($sm->get('Voodoo773Localization\Entity\Career'));
                    $form->setInputFilter($sm->get('Voodoo773Localization\Form\CareerFilter'));
                    $form->setHydrator(new ClassMethods());
                    
                    return $form;
                },
                
                'Voodoo773Localization\Mapper\CareerMapper'  => function (ServiceLocatorInterface $SL) {
                    $carierMapper = $SL->get('Voodoo773Localization\Mapper\Career');
                    $carierMapper->setDbAdapter($SL->get('Zend\Db\Adapter\Adapter'));
                    $carierMapper->setEntityPrototype($SL->get('Voodoo773Localization\Entity\Career'));
                    $carierMapper->setHydrator(new ClassMethods());
                    
                    return $carierMapper;
                },
                'Voodoo773Localization\Mapper\ContactMapper' => function (ServiceLocatorInterface $sm) {
                    $contactMapper = $sm->get('Voodoo773Localization\Mapper\Contact');
                    $contactMapper->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                    $contactMapper->setEntityPrototype($sm->get('Voodoo773Localization\Entity\Contact'));
                    
                    return $contactMapper;
                },
                'Voodoo773Localization\Mapper\UploadMapper'  => function (ServiceLocatorInterface $sm) {
                    $uploadMapper = $sm->get('Voodoo773Localization\Mapper\Upload');
                    $uploadMapper->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                    $uploadMapper->setEntityPrototype($sm->get('Voodoo773Localization\Entity\Upload'));
                    $uploadMapper->setHydrator(new ClassMethods());
                    
                    return $uploadMapper;
                },
                'Voodoo773Localization\Factory\Upload'       => function (ServiceLocatorInterface $sm) {
                    $form = $sm->get('Voodoo773Localization\Form\Upload');
                    $form->setObject($sm->get('Voodoo773Localization\Entity\Upload'));
                    
                    return $form;
                },
            ],
        ];
    }
    
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ],
            ],
        ];
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getServiceManager()->get('viewhelpermanager')
          ->setFactory('routeName', function ($sm) use ($e) {
            $viewHelper = new View\Helper\RouteName($e->getRouteMatch());
    
            return $viewHelper;
          });
        
        $e->getApplication()
          ->getEventManager()
          ->attach(MvcEvent::EVENT_ROUTE, function ($e) {
              $this->setLocalization($e);
          }, -1000);
        
        // редирект с home на application
        /* TODO подтягивать локаль и перенаправлять на главную с соответствующим языком */
        $e->getApplication()->getEventManager()->getSharedManager()
          ->attach(
              'Zend\Mvc\Controller\AbstractActionController',
              MvcEvent::EVENT_DISPATCH,
              function ($e) {
                  $controller = $e->getTarget();
                  $lang = $controller->params()->fromRoute('lang', 'ru');
                
                  if ('home' === $e->getRouteMatch()->getMatchedRouteName()
                      || 'application' === $e->getRouteMatch()->getMatchedRouteName()
                  ) {
                      $controller->plugin('redirect')->toRoute(
                          'localization',
                          [
                              'lang' => $lang,
                          ]
                      );
                  }
              }
          );
        
        //            $e->getApplication()->getEventManager()->getSharedManager()
        //                ->attach('Zend\I18n\Translator\Translator', Translator::EVENT_MISSING_TRANSLATION,
        //                    function ($e) {
        //                        $locale = $e->getParam('locale');
        //                        $fallBackLocale = $e->getTarget()->getFallbackLocale();
        //                        if ("ru_RU" !== $locale && is_null($fallBackLocale)) {
        //                            $e->getTarget()->setFallBackLocale('en_US');
        //                        }
        //                    });
        //        }
    }
    
    public function setLocalization(MvcEvent $e)
    {
        $manager = new SessionManager();
        $containerSession = new ContainerSession();
        
        $baseLanguage = $containerSession->lang ?: 'ru';
        $containerSession->lang = $e->getRouteMatch()->getParam('lang', $baseLanguage);
        
        $config = $e->getApplication()->getServiceManager()->get('config');
        //        $translator = $e->getApplication()->getServiceManager('translator');
        
        $locale = $config['lang'][$containerSession->lang]['locale'];
        
        $translator = $e->getApplication()->getServiceManager()->get('MvcTranslator');
        
        if (isset($config['lang'][$containerSession->lang])) {
            // устанавливаем локаль на определенный язык
            $translator->setLocale($locale);
        } else {
            // устанавливаем локаль на неизвестный/не существующий язык - по дефолту
            $lang = array_shift($config['lang']);
            $translator->setLocale($lang['locale']);
        }
        
        if ($containerSession->lang == 'pt') {
            $containerSession->lang = 'pt_BR';
        }
        
        $translator->addTranslationFile(
            'phpArray',
            './vendor/zendframework/zend-i18n-resources/languages/' . $containerSession->lang . '/Zend_Validate.php'
        );
        
        AbstractValidator::setDefaultTranslator($translator);
    }
    
    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'calcLang'            => 'Voodoo773Localization\View\Helper\CalcLang',
                'lang'                => 'Voodoo773Localization\View\Helper\Lang',
                'locale'              => 'Voodoo773Localization\View\Helper\Locale',
                'currentRoute'        => 'Voodoo773Localization\View\Helper\CurrentRoute',
                'OrderExchangeIfAuth' => 'Voodoo773Localization\View\Helper\OrderExchangeIfAuth',
                //                'getMatchedRoute'     => 'Voodoo773Localization\View\Helper\RouteMatch',
            ],
        ];
    }
    
}
