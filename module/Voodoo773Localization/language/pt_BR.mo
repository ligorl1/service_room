��    �      �  �   �      �  8   �  "   �  H        N  2   `  7   �  M   �       ,   2     _  6   {    �  {  �  �  0    )     .  <   O  �  �  [        r#     �#  #   �#     �#     �#     �#     �#     �#     �#     $     %$      7$  
   X$  
   c$     n$  	   |$     �$     �$     �$     �$     �$     �$     %     %     -%     <%     E%     `%     t%     �%     �%     �%     �%      �%     �%    &  	   "'     ,'     ;'     J'     R'     e'     s'     �'     �'     �'     �'     �'     �'     �'  	   �'     �'     (     %(     ,(     :(     J(     \(     l(     |(     �(     �(     �(     �(     �(  	   �(  "   )     +)     J)     `)     v)     |)     �)     �)     �)     �)     �)     �)     �)     �)     *     *     *     2*     E*     Z*     l*     �*     �*     �*     �*     �*     �*     �*  7   +     H+     V+     i+     �+     �+     �+     �+     �+     �+  y  �+  c  L0     �2     �2     �2  %   �2     3      03  &  Q3  :  x5     �6     �6  �  �6  �   j8    ]9  0   m:  a   �:  �    ;     �;     �;  �  �;  w  �@     GE     VE     cE     zE    �E  �  �G    aI  .   cK  K  �K  �  �N     vP  !   �P     �P  �  �P  '   HR  b   pR  �  �R  �  rT     W  2   *W     ]W  6   tW  {   �W     'X     FX  4   eX  .   �X  (   �X  �   �X  H  �Y  ,   �Z     [     [  �  '[  =   ]     P]     p]  E   �]  ,  6^  A   c`    �`  !   �b     �b  @   �b      c  (   0c  #   Yc  2   }c     �c     �c     �c     �c  d  d  �   jf    %g  �   Dh  �   7i     �i    �i  �  l     �m     �m     �m     �m      n     n     n     n     4n     Mn     hn  "   {n     �n     �n     �n     �n     �n     �n  5   o     Do     ]o     yo     �o     �o     �o     �o     �o     �o     �o     p     p     /p  	   7p  %   Ap     gp  ,  {p     �q     �q     �q     �q     �q     r     r     &r  #   8r     \r     kr     {r  
   �r     �r     �r     �r     �r     �r     �r      s     s     0s     Ks     _s     ss  "   �s     �s     �s     �s  	   �s  $   �s  /   t     Dt     Zt     st     |t     �t     �t     �t  	   �t     �t     �t     �t     �t     u     #u     ,u     Eu     Uu     ou     �u     �u     �u     �u     �u     �u     v     -v  =   Ev     �v     �v     �v     �v     �v     �v  
   �v  	   	w     w  f  w  ;  �y  	   �z     �z     �z     �z      �z     {  6  7{  �   n|     }     }  �   }  �   �}  �   r~       1     y   M     �     �  �  �  �  ��     ;�  
   C�     N�     V�    o�  .  x�    ��     È  �  ׈  �   w�     Y�     i�     ��  �   ��     ^�  -   u�     ��  D  č     	�     �     +�      8�  B   Y�     ��     ��  N   ��     �     '�  V   8�  �   ��     #�  	   9�     C�    K�     d�     j�  >   }�  %   ��  G  �  !   *�         =       }       N   q   �      ]   '   D      L           7       A   �       T   r   S   �      Z       i   �          f   E   �          (   �   m                  �       :   c   0           �   �   h   W   �   �       V      b           R   <   �       �   �   6   _   �      �   x       �   w   �   p   U       �   �              X   g       H   )   �       �       e   �   #   8       3   J       �   O   \   �   &                     ^   `   ?   ~   M       	      !   �   �   �   y      u   �   Q   �   5   $   a      K   k           d   �   *       �      �   �   �   �              F   �       P   �                  �   j           2   I   /   1             4              �   �   t       �   9   �                     �   �   �   {   �      �   z   �                  �       "       [   �   �   +   o           s   ,   ;       v   �          �   G       C   B       �   l       �   �   
      Y      n       %   �   >   @   |       .   -       �          10. Модификация данной Политики 11. Связаться с нами 2. Неличная информация о нашем Веб-сайте 2Furniture (Used) 3. Использование информации 4. Раскрытие личной информации 5. Обзор и Доступ к Вашей личной Информации 6. Выбор/Отказ 7. Ссылки на другие сайты 8. Безопасность 9. Детская конфиденциальность ACEX24 - быстро развивающаяся

            логистическая информационная компания с

            более чем двадцатилетним опытом в сфере

            логистики. Предлагаемый сервис, который мы

            развиваем, в корне перевернет мир логистики,

            чтобы лучше обслуживать миллионы

            пользователей, которые остаются

            недовольными современными реалиями

            индустрии. Со штаб-квартирой в Москве,

            Россия, мы - частная компания, работающая,

            чтобы помочь бизнес-компаниям и частным

            лицам стать более продуктивными и

            квалифицированными. ACEX24 может изменить эту политику
конфиденциальности в любое время без
предварительного уведомления, разместив
обновленную политику конфиденциальности на
этом Сайте с указанием даты последнего
обновления. ACEX24 может получить определенную
комплексную информацию, даже известную как
демографические сведения и данные профиля
с нашего Веб-сайта. Эта информация помогает
нам в сборе сведений, таких как количество
Посетителей нашего Веб-сайта и количества
просмотренных ими страниц. ACEX24 не предоставляет информацию
несовершеннолетним и не принимает
информацию от несовершеннолетних детей.
Если Вы обнаружили, что Вы ошибочно
получили информацию от
несовершеннолетнего ребенка, Мы будем
немедленно прилагать все усилия, чтобы
удалить информацию из наших записей ACEX24 никогда не продаст, не сдаст, не
поделится, не обменяет Вашу личную
информацию без Вашего согласия, за
исключением случаев, изложенных в данном
документе. ACEX24 политика конфиденциальности ACEX24 серьезен в частном характере
персональной информации, такой как имя,
адрес, телефон, электронная почта,
полученной или предоставленной
посетителями ("Гость", "Вы", "Ваш" или
"Пользователь") нашему веб-сайту
www.acex24.net ("Веб-сайт"). Эта политика
конфиденциальности описывает пути
предоставления Вами Нам Вашей
персональной информации, виды личной
информации, которые Мы собираем, и как
ACEX трактует информацию, которую Мы
собираем, когда Вы посещаете Веб-сайт и/или
регистрируетесь как Пользователь. ACEX24, его филиалы и другие поставщики
услуг могут предоставить Вашу персональную
информацию в ответ на действительный
юридически законный запрос, применимый к
законодательству Российской Федерации. Мы
также можем раскрыть личную информацию,
когда это необходимо для создания,
осуществления или защиты правовых
притязаний, чтобы исследовать или
предотвратить фактическую или
предполагаемую потерю или вред людям или
собственности, или другому, допускаемое
законом. Accessorial charges Add Insurance Agriculture (Fruits and Vegetables) Air All Apparel Arts & Crafts Automobile & Motorcycle Parts Automobiles & Motorcycles Beauty & Personal Care Bottled Beverages Bottled Products (Non-beverages) Boxcar 50' Boxcar 60' Branded Goods Chemicals Coils Commodity Value (USD) Computer Hardware & Software Computers/Electronics Construction Equipment Construction Materials Construction Modules Consumer Electronics Container Type Currency Customs Or In-Bond Freight Destination Airport Destination City Destination Countries Destination Port Duty Electrical Equipment & Supplies Electronic Components & Supplies Fashion Accessories Find out market rate for Ocean and Rail, Truck and Air estimates from anywhere to anywhere in the world. Upon seeing the estimate, you have two options on how to proceed, should you decide to do so; either post in Exchange or get a Reservation and someone will contact you. Have fun! Fine Arts Flatbead 100MT Flatbead 167MT Flatbed Food (Frozen Meat) Food (Frozen) Food (Perishable) Fragile Goods Freight Furniture (New Branded) Furniture (Used) General Merchandise Gross weight Hardware Hazardous Hazardous Shipment Health & Medical Supplies Height Home & Garden Home Appliances Hopper 50' Closed Hopper 50' Open Household Goods Household Goods (Used) Inside/Limited Access Delivery Inside/Limited Access Pickup Length Lights & Lighting Luggage, Bags & Cases Machinery Measurement & Analysis Instruments Mechanical & Fabrication Parts Minerals & Metallurgy New or Used Machinery Ocean Office & School Supplies Origin Airport Origin City Origin Port Packaging & Printing Partner Precision Instruments Rail Refrigerated Refrigerated 52 Reserve Now Residential Delivery Residential Pickup Return to Calculator Rubber & Plastics Security & Protection Select Commodity Select load Select locations Select method of shipping Shoes & Accessories Sports & Entertainment Steel Sheets, Coils & Bars Subject to additional fees such as taxes, duties, etc.. Tank 12K Gls2 Telecommunications Textiles & Leather Products Timepieces, Jewelry, Eyewear Tools Toys & Hobbies Transportation Truck Width В дополнении к сбору личной и неличной
информации, ACEX24 может собирать
неличную, сводную информацию об
использовании данного Веб-сайта. Эта
информация не является личной и будет
использоваться только для поиска того, как
зарегистрированные Пользователи используют
наш сервис и Веб-сайт. Например, эта
информация будет говорить Нам сколько
времени проводит Пользователь на сайте, что
находят для себя Пользователи на сайте, и на
каких сайтах находятся наши пользователи. Сбор
подобной информации позволит Нам, среди
всего прочего, подготовиться к требованиям
нагрузки трафика и эффективной доставки
наших продуктов и услуг. В рамках сервиса, ACEX24 может создать
связи, позволяющие Вам получать доступ к
сторонним веб-сайтам. ACEX24 не несет
ответственности за содержимое, которое
появится на этих сайтах и не потверждает эти
сайты. Пожалуйста, обратитесь к политике
конфиденциальности этих сайтов, чтобы
определить, как относиться к пользовательской
информации. Ваш email Ваш комментарий Ваше имя Ведомость индустрии Войти как клиент Войти как партнёр Вся информация, описанная выше хранится на
ограниченных серверах баз данных. Тем не
менее, Вы не должны предоставлять
конфиденциальную информацию на этом Веб-
сайте. Мы поддерживаем разумные
технические, физические и административные
меры предосторожности, чтобы помочь
защитить личную информацию. Вы должны написать Нам электронное письмо
через "Связаться с Нами" на нашем Веб-сайте.
Мы запросим Вашу контактную информацию,
чтобы ответить на Ваш запрос или
комментарий. Добавить место Другое Если Вы захотите получать Анонсы Веб-сайта,
Пресс-релизы, Презентации, Службу
уведомлений или другие уведомления по
электронной почте, Мы запросим Выше имя и
электронную почту, чтобы предоставить Вам
эти уведомления. Если Мы присылаем Вам электронные письма
с информацией о новых продуктах или услугах,
Вы сможете отказаться от будущих
уведомлений. Если у Вас есть какие-либо вопросы или
комментарии об этой Политике
Конфиденциальности, или нашем Веб-сайте в
общем, пожалуйста, свяжитесь с нами по: Запрос торговой стоимости Заявление о конфиденциальности компании-партнера ACEX Здесь ACEX24 было бы интересно узнать, что выхотите сказать. Напишите нам пару строк или скажите привет Идет отправка... Имя Как большинство Веб-сайтов, ACEX24
пользуется сервером “cookies”. Cookie - это
маленький текстовой документ, который часто
включает анонимный уникальный
идентификатор. Когда Вы посещаете веб-сайт,
компьютер сайта запрашивает Ваш компьютер
разрешение сохранить этот файл в части
Вашего жесткого диска, который специально
предназначен для cookies. Каждый веб-сайт
может послать собственный cookie Вашему
браузеру, если предпочтения Вашего браузера
позволяют сделать это, но (чтобы защитить
Вашу приватность) Ваш браузер позволяет
веб-сайту открыть доступ к cookies, которые он
уже Вам посылал, а не те cookies,
отправленные Вам другими сайтами. Вы
можете управлять или отключить cookies
инструментами Вашего браузера Как только Вы загрузите Наш Веб-сайт, он
использует cookies, чтобы отличить Вас от
других пользователей, чтобы уберечь Вас от
просмотра ненужной рекламы или требуя,
чтобы Вы зарегистрировались там, где не
нужно. Cookies, в сочетании с нашими лог
файлами веб-сервера, позволяют Нам
вычислить суммарное количество людей,
посещающих наш Веб-сайт и проследить, какие
его части являются самыми популярными. Это
помогает Нам собрать обратную связь, чтобы
постоянно улучшать наш Веб-сайт и лучше
обслуживать наших клиентов. ACEX никогда не
использует cookies для получения информации
с компьютера, которая не связана с нашим
Сайтом или услугами. Карьера Клиент Комментарий Контакты Мы можем передать (или иным способом
распространить) Вашу персональную
информацию нашим филиалам и другим
третьим лицам, которые предоставляют услуги
от нашего имени. Ваша персональная
информация может быть сохранена и
обработана нашими филиалами и другими
поставщиками услуг третьей стороны. Мы можем поделиться этой информацией с
другими, такими, как рекламодатели, которые
заинтересованы в размещении рекламы на
нашем Сайте, в анонимной форме, что
означает, что информация не будет содержать
каких-либо персональных данных о Вас. Мы также можем использовать эти сведения,
чтобы улучшить наш Сайт и/или применить
Ваш опыт на нашем Сайте, показав Ваше
содержимое, в котором Мы можем быть
заинтересованы, и отображая содержимое, в
котором Вы можете быть заинтересованы. Мы
получим эти данные через технологии "Cookie". Начните пользоваться ACEX24 Наша команда разнообразна, сильно

            мотивирована, трудолюбива и позитивно

            конкурентноспособна. Мы создали счастливый,

            быстро растущий, крепкий стартап рабочего

            пространства, который стремится к

            удовлетворению наших высоких внутренних

            ожиданий. Кандидаты должны быть

            увлеченными перфекционистами, которые не

            остановятся ни перед чем, чтобы прекрасно

            выполнить свою работу. Иные нам не подходят. Нашим поставщикам услуг предоставляется
информация, которая необходима им для
выполнения поставленных перед ними задач, и
мы не разрешаем им раскрывать личную
информацию для их собственного продвижения
или других целей. Опасный груз Особенности груза Пароль Персональная информация относится к любой
информации в отношении одного из
идентифицируемой личности. Эта информация
может быть получена через этот Веб-
сайт,www.acex24.net, или одним из наших веб-
сайтов, соответственно: Подтверждение пароля Правовые вопросы, соблюдение и
предотвращение потерь Предоставляя ACEX24 любую информацию, Вы
соглашаетесь с использованием нами
информации, изложенной в настоящей
Политике Конфиденциальности. Если Вы не
согласны с условиями ниже, пожалуйста, не
предоставляйте Нам эту информацию Продолжая, я соглашаюсь с тем, что компания ACEX или ее представители
                    могут связаться со мной при помощи эл. почты, телефона 
                    или СМС (включая автоматические средства), воспользовавшись указанными 
                    мной эл. адресом или номером телефона, в том числе в маркетинговых целях. 
                    Я подтверждаю, что прочитал(а) и понял(а) соответствующее  Рассчитать Расчет стоимости перевозки Регистрация Сбор персональной информации Свяжитесь с нами, если Вы заинтересовались
работой в нашей команде. Связаться с Нами Связаться с нами Следите за своим заработком. Соглашение Услуг Сервиса Создать новый аккаунт Станьте партнером ACEX24 и зарабатывайте хорошие деньги в качестве независимого подрядчика. Также могут быть и другие места на нашем
Веб-сайте, где Мы можем запросить Вашу
личную информацю. Мы будем запрашивать
личную информацию только когда это
действительно необходимо Текущая Рыночная Ставка Телефон: Тема У Вас есть право на доступ, обновление или
исправление неточностей в Вашей личной
информации под нашим заключением и
контролем, с учетом некоторых исключений,
установленных законом. Вы можете запросить
доступ, связавшись с нами, используя
контактную информацию ниже. Уведомления по электронной почте Уже есть аккаунт? Управляйте способами оплаты, просматривайте историю и многое другое. Химки, 141400, Московская область, Россия Чтобы разместить Вас с Вашим запросом
торговой стоимости, предоставить Вам
дополнительные услуги, а также отправлять
информацию об услугах ACEX24 в Вашем
местоположении, мы можем запросить Ваше
имя, географические координаты, номер
телефона и другую личную информацию,
которую Вы прямо не предоставляете. нуждается в таких партнерах, как вы. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-04-18 17:21+0300
PO-Revision-Date: 2017-04-18 17:24+0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.12
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: ..
 10. Modificação desta Política 11.Póngase nos 2.Informação informações não pessoais sobre o nosso website Móveis (usado) 3 Divulgação de Informações Pessoais Divulgación informações pessoais 5.Revisar e acesso às suas informações pessoais 6. Escolha / Opt-Out 7. Links para outros sites 8. Segurança 9. Privacidade menores informações ACEX24 é uma logística global e os preços de frete de crescimento rápido e com mais de 20 anos de experiência na indústria e nos serviços de logística, temos desenvolvido transporte.Los nos colocou na vanguarda do exercício de logística global com A fim de melhor servir os milhões de usuários que foram "insatisfeitos" com a realidade atual com base industria.Con Moscow, Russia, somos uma empresa privada focada em ajudar empresas e indivíduos envolvidos no mercado de transportes e ajudar na eficácia e eficiência de suas necessidades de negócios e de transporte de carga global. ACEX24 pode alterar estas políticas de privacidade a qualquer momento, sem aviso prévio, publicando novas regras atualizadas neste site, incluindo a data da atualização mais recente. ACEX24 poderia obter determinadas informações agregadas não pessoalmente identificáveis, também conhecido como perfil demográfico e dados, ousar no nosso site. Esta informação ajudará na coleta de dados, tais como quantos visitantes temos ao nosso site e as páginas visitadas. ACEX24 não vende com menores de idade nem intencionalmente informações de crianças. Se descobrirmos que temos recebido informações de um engano menor, vamos nos esforçar para eliminar imediatamente as informações de nossos registros. ACEX24 nunca vender, alugar, compartilhar, comercializados ou troca de informações sem o seu consentimento, salvo disposição em contrário nele previsto. ACEX24 Política de Privacidade ACEX24 é levado a sério pela privacidade de suas informações pessoais, como nome, endereço, número de telefone e endereço de e-mail, obtido a partir, ou fornecidas pelos visitantes ("Visitante", "Você", "Seu" ou "Usuário") www.acex24.net nosso website ("Website") . Esta política de privacidade descreve a maneira em que você fornecer suas informações pessoais, os tipos de informações pessoais que coletamos e como as informações Freight Paradigm é obtida quando você visitar o site e / ou o utilizador registado. ACEX suas afiliadas e outros prestadores de serviços podem fornecer suas informações pessoais em resposta a um inquérito ou ordem legalmente válida aplicável a lei dos EUA Podemos também divulgar informações pessoais como necessário para estabelecer o exercício ou a defesa de ações judiciais, para investigar ou prevenir suspeito perda real, o prejuízo para as pessoas, a propriedade ou conforme permitido por lei. DESPESAS ADICIONAIS Adicionar Seguro Agricultura (Frutas e Legumes) Ar Todos Roupa Belas artes Automóveis, Motos e Peças Automóveis e Motociclos Beleza e Cuidados Pessoais bebida engarrafada Produtos engarrafados (No bebidas) Vagon Fechado 50 &#039; vagão 60 &#039; Produtos de marca Produtos químicos Bobinas Valor de mercadoria (USD) Programas de computador e componentes de computadores Computadores eletrônico Equipamento de construção Construção Construction Modules Consumer Electronics Tipo de Container Valuta Alfândega Carga e Ancorado Aeroporto de Destino Cidade de destino Cidade de destino Porto de Destino Tariffa Elétrico Componentes Elétricos e Equipamentos Acessórios de Moda Conheça estimativas de taxa de mercado para Oceano de qualquer lugar para qualquer lugar do mundo, e Rail e caminhão. Ao ver a estimativa, você tem duas opções sobre como proceder, caso decida fazê-lo, ou pós no Exchange ou ter uma reserva e alguém entrará em contato com você. Divirta-se!! Belas Artes Plataforma Vagon 100MT Plataforma Vagon 167mt Plataforma Vagon Food (Frozen Meat) Food (Frozen) Food (Perishable) Produtos frágeis Растаможенный груз Móveis (Nova) Móveis (usado) General Merchandise Peso Bruto Ferramentas Materiais perigosos Artigos perigosos Insumos para Saúde e Medicina Altura Casa e Jardim Eletrodomésticos Vagon Hopper 50 &#039;Fechado Vagon Hopper 50 &#039;open Produtos para o Lar Produtos para o Lar Exterior / Acesso limitado Interior / limitada Entrega Acesso Comprimento Luzes e Destaques Malas, bolsas e malas Mecânica Instrumentos de Medição e Analicis Peças Mecânicas e Suprimentos de fabricação Minerais e Metalurgia novas máquinas e Uasada Maritimo Escritório e Material Escolar Aeroporto de origem Origem da Cidade Porto de origem Embalagem Parceiro Instrumentos presicion Trem Frigorífico Frigorífico 52 &#039; Book Now Prestação de Serviços Pegue Endereço Retornar para calculadora Plásticos e Borracha Protecção e Segurança Selecionar mercadoria SELECIONE CARGA SELECIONE UMA LOCALIZAÇÃO SELECIONE MÉTODO DE ENVIO Calçados e Acessórios Esportes e entretenimento Chapas, Bobinas e Bares Sujeitos a taxas adicionais de serviço, impostos, taxas, etc GLS Tanque 12K Telecomunicações Têxteis e produtos de couro Relógios, Jóias, Óculos Ferramentas Brinquedos e Hobbies Transporte Caminhão Largura Além de coletar dados pessoais e não pessoais, ACEX24 podem coletar informações não pessoais ou agregadas sobre o uso deste website. Esta informação não é pessoalmente identificável e só serão utilizadas para descobrir como usuário registrado com o nosso serviço e website. Por exemplo, essa informação vai nos dizer a duração da visita em nosso site para que nossos usuários estão vendo as páginas que nossos usuários visitam. A recolha desta informação nos permite, entre outras coisas, se preparar para a demanda de tráfego e para atender as expectativas de nossos produtos e serviços. Como parte do serviço, ACEX24 pode criar links que permitem o acesso a sites de terceiros. ACEX24 não é responsável pelo conteúdo que aparece em tais sites nem endossa. Por favor, consulte as declarações de privacidade dos sites individuais, a fim de determinar como eles tratam as informações do usuário. Seu email Seu comentário Seu nome Diretório indústria Início de sessão de utilizador Parceiro de sessão de início Todas as informações descritas acima está armazenado em servidores de dados restritos. No entanto, você não deve fornecer nenhuma informação confidencial neste website. Mantemos salvaguardas razoáveis ​​as medidas técnicas, físicas e administrativas para ajudar a proteger informações pessoais. Um e-mail através do "Fale Conosco" link em nosso site, pedimos para suas informações de contato para responder às suas perguntas ou comentários. + item Outros Se você quiser receber anúncios de sites, press releases, apresentações, atualizações de serviço ou alertas de e-mail, solicitamos o seu nome e e-mail para fornecer esses alertas para você. Se você nunca enviar alertas por e-mail ou informações sobre novos produtos ou serviços, você terá a oportunidade de recusar anúncios ulteriores. Se você tiver quaisquer perguntas ou comentários sobre a nossa política de privacidade ou o nosso site em geral, por favor contacte-nos em: Informações do concurso Empresa Declaração de Privacidade parceiro ACEX Em ACEX24 que gostaria de ouvir sobre sua experiência com nosotros.Envíenos seus comentários ou apenas uma saudação. Enviando… Nome Como a maioria dos sites, ACEX24 faz uso de "cookies". Os cookies são pequenos documentos de texto, o que muitas vezes inclui um identificador anônimo único. Quando você visita um site onde o computador desse local pede seu computador para a permissão armazenar esta lima em uma parte do seu disco rígido especificamente designados para cookies. Cada site pode enviar o seu próprio cookie para o seu navegador se as preferências do seu navegador o permitem, mas (para proteger sua privacidade) seu browser permite somente um Web site para acessar os cookies que já foram enviados, não os cookies enviados a você por outros sites. Você pode ativar ou desativar os cookies usando as ferramentas de seu navegador. Enquanto você estiver navegando em nosso site, o site usa cookies para diferenciá-lo de outros usuários, a fim de evitar ver anúncios desnecessários ou que você precise entrar em outra sessão é necessário para a segurança. Cookies, em conjunto com os arquivos de log do nosso servidor web, nos permitem calcular o número total de pessoas que visitam nosso site e quais partes do site são mais populares. Isso nos ajuda a recolher feedback para melhorar constantemente nosso Web site e melhor servir os nossos clientes. ACEX nunca usei cookies para recuperar informações de um computador que não está relacionado ao nosso site ou serviços. Emprego Utilizador Comente Informações de Contato Podemos transferir (ou não) suas informações pessoais a nossos parceiros e outros terceiros que prestam serviços em nosso nome. As suas informações pessoais podem ser realizadas e processadas por nossos membros e outros prestadores de serviços de terceiros. Nós podemos compartilhar essas informações com outros, tais como os anunciantes interessados ​​em publicidade em nosso site, em forma agregada, anônimo, o que significa que a informação não conterá qualquer informação de identificação pessoal sobre você ou sobre qualquer outra pessoa. Podemos usar essas informações para melhorar nosso site e / ou adequar a sua experiência no nosso site, mostrando-lhes o conteúdo que acreditamos poderia estar interessado, e exibir o conteúdo de acordo com suas preferências. Nós obter essa informação através de "cookies". Comece com a ACEX24 Nossa equipe é diversa, altamente motivado, trabalhador, competitivo e positivo. Criamos um ambiente de trabalho com um ritmo acelerado, o compromisso de atender às nossas expectativas. Os candidatos que desejam se juntar à nossa equipe deve estar de acordo com a descrição do apaixonado que são perfeccionistas e vai parar em nadapara completar um grande trabalho a fazer. Todos os outros precisam se aplica. Nossos prestadores de serviços têm as informações necessárias para desempenhar suas funções designadas, não estamos autorizados a utilizar ou divulgar informações pessoais para seu próprio marketing ou outros fins. Dangerous goods Características da carga Senha A informação pessoal refere-se a qualquer informação pessoalmente identificável. Esta informação pode ser obtida através do nosso site, www.acex24.net,, ou qualquer um dos nossos outros sites, como segue: Confirmação de senha Jurídico, Compliance e Prevenção de Perda: Quando os usuários compartilham suas informações pessoais com frete ACEX24 concorda que podemos usar essa informação, tal como estabelecido em nossa política de privacidade. A falta de concordar com as condições estabelecidas acima, por favor, não nos fornecer esta informação. Ao continuar, concordo com o fato de que a empresa ACEX ou seus representantes podem entrar em contato comigo por e-mail. mail, telefone ou SMS (incluindo meios automática), usando o acima de mim e-mail. endereço ou número de telefone, incluindo fins de marketing. Confirmo que tenho lido e entendido com o correspondente Obter Frete Сalculadora de frete inscrever-se Coleta de Informações Pessoais EM CONTATO CONOSCO se você está interessado em trabalhar conosco Envie-nos uma mensagem Fale Conosco Encontre as ferramentas necessárias para acompanhar o seu sucesso na estrada. Prestadores de Arranjos Criar nova conta Torne-se um parceiro ACEX24 e ganhar um bom dinheiro como um contratante independente. Pode haver outras áreas de nosso site, onde pedimos a sua informação pessoal. Basta perguntar a essas informações pessoais quando necessário. Taxa Do Mercado Atual Telefone: Sujeito O usuário tem o direito de rever, actualizar e corrigir imprecisões nas suas informações pessoais sob nossa custódia e controle, sujeito a certas exceções previstas em lei. Você pode solicitar o acesso entrando em contato conosco usando as informações de contato abaixo. Email Já tem uma conta? Efetue a gestão das suas opções de pagamento, e muito mais. Khimki, Moscow region, 141400, Russia Para dar o seu lance, você será oferecido serviços adicionais, como o envio de informações sobre o produto frete ACEX24 à sua localização, podemos solicitar seu nome, endereço, número de telefone e e-mail. Freight paradigma não recolhem qualquer outra informação pessoal que você deseja não prever expressamente. precisam de parceiros como você. 